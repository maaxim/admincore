<?php

return [
    'links' => [],
    'rules' => [],

    'resource_path' => '',
    'resources' => [
        'css' => [
            //
        ],
        'js' => [
            //
        ]
    ],

    'roles_without_interaction' => [1, 2],

    'errors_report' => false,
    'send_email_report' => false,
    'emails' => 'programmer@yandex.ru',

    'vue-devtool' => false,
    'vue-devtool-script' => '<script src="http://localhost:8098"></script>',

    'preloader' => false,
    'breadcrumbs' => true
];
