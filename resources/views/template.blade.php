<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @if(config('admin.vue-devtool') == true)
        {!! config('admin.vue-devtool-script') !!}
    @endif
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? 'Заголовок' }} | AdminPanel</title>
    <link rel="icon" href="{{ Admin::getResource('/images/AdminLogo.png') }}">
    {!! Admin::renderResources() !!}
</head>

@php(Admin::setLastVisit())

<body class="hold-transition sidebar-mini
    {{ Admin::getUserData('customize.header_fixed', true) ? ' layout-navbar-fixed' : '' }}
{{ Admin::getUserData('customize.sidebar_collapse', false) ? ' sidebar-collapse' : '' }}
{{ Admin::getUserData('customize.layout_fixed', true) ? ' layout-fixed' : '' }}
{{ Admin::getUserData('customize.footer_fixed', true) ? ' layout-footer-fixed' : '' }}
{{ Admin::isThemeDark() ? ' dark-mode swal-dark' : '' }}">
@if(Session::has('success'))
    <script>
        window.onload = function () {
            Swal.fire({
                icon: 'success',
                title: '{{ Session::get('success') }}'
            });
        };
    </script>
@elseif(Session::has('error'))
    <script>
        window.onload = function () {
            Swal.fire({
                icon: 'error',
                title: '{{ Session::get('error') }}'
            });
        };
    </script>
@endif
<div class="wrapper">
    @if(config('admin.preloader') == true)
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__wobble animate__delay-1s animate__animated"
                 src="{{ Admin::getResource('images/AdminLogo.png') }}" alt="AdminLogo" height="60" width="60">
        </div>
    @endif

    @include('admin::elements.header')
    @include('admin::elements.sidebar')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ $title ?? 'Заголовок' }}</h1>
                        <small>{{ $small ?? '' }}</small>
                    </div>
                    <div class="col-sm-6">
                        @if(config('admin.breadcrumbs') == true)
                            <ol class="breadcrumb float-sm-right">
                                @foreach((new \maaxim\admincore\Classes\Breadcrumbs())->get() as $item)
                                    <li class="breadcrumb-item {{ $item['active'] ? 'active' : '' }}">
                                        @if($item['active'])
                                            <a href="{{ $item['href'] }}">{{ $item['title'] }}</a>
                                        @else
                                            {{ $item['title'] }}
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container-fluid">
                @section('content')
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>

                                    <p class="card-text">
                                        Some quick example text to build on the card title and make up the bulk of the
                                        card's
                                        content.
                                    </p>

                                    <a href="#" class="card-link">Card link</a>
                                    <a href="#" class="card-link">Another link</a>
                                </div>
                            </div>

                            <div class="card card-primary card-outline">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>

                                    <p class="card-text">
                                        Some quick example text to build on the card title and make up the bulk of the
                                        card's
                                        content.
                                    </p>
                                    <a href="#" class="card-link">Card link</a>
                                    <a href="#" class="card-link">Another link</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="m-0">Featured</h5>
                                </div>
                                <div class="card-body">
                                    <h6 class="card-title">Special title treatment</h6>

                                    <p class="card-text">With supporting text below as a natural lead-in to additional
                                        content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>

                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h5 class="m-0">Featured</h5>
                                </div>
                                <div class="card-body">
                                    <h6 class="card-title">Special title treatment</h6>

                                    <p class="card-text">With supporting text below as a natural lead-in to additional
                                        content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @show
            </div>
        </div>
    </div>
    <aside class="control-sidebar control-sidebar-dark">
    </aside>
    @include('admin::elements.footer')
</div>
</body>
</html>
