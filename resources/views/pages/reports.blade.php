@extends('admin::template')

@section('content')
    <div class="card card-outline card-danger" id="report">
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width:1%">#</th>
                    <th>Ошибка</th>
                    <th style="width: 15%">Статус</th>
                    <th style="width: 15%">Дата</th>
                    <th style="width:1%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($reports as $item)
                    <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{ substr($item->message, 0, 200) }}</td>
                        <td>
                            {{ $item->read
                                ? ($item->fixed ? 'Ошибка исправлена' : 'Ошибка не исправлена')
                                : 'Не прочитано' }}
                        </td>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            <a href="{{ route('AdminReport', ['id'=>$item->id]) }}"
                               class="btn btn-primary btn-xs">
                                <i class="fas fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-end">
                <button type="button" class="btn btn-outline-danger" @click="clearTable">
                    <i class="fas fa-trash-alt"></i>
                    Очистить логи
                </button>
            </div>
        </div>
    </div>
@endsection
