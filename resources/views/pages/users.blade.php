@extends('admin::template')

@section('content')
    <div class="card card-solid card-outline card-success card-users">
        <div class="card-header">
            <button class="btn btn-success btn-flat" @click="addShowModal">
                Добавить
                <i class="fas fa-plus"></i>
            </button>
        </div>
        <div class="card-body pb-0">
            <div class="d-flex justify-content-between">
                <v-input
                        class="form-group"
                        placeholder="Поиск: имя..."
                        v-model="search"
                        name="search"
                ></v-input>
                <div class="form-group">
                    <select2 placeholder="Роль пользователя" v-model="roleSearch">
                        <option value="all">Все</option>
                        <option v-for="role of {{ $roles }}"
                                :value="role.role_id">
                            @{{ role.role_name }}
                        </option>
                    </select2>
                </div>
            </div>

            <div class="row d-flex align-items-stretch">
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch"
                     v-for="user of list"
                >
                    <div class="card w-100">
                        <div class="card-header text-muted border-bottom-0">
                            @{{ user.role_name }}
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="lead">@{{ user.name }}</h2>

                                    <div class="mb-1 d-flex" v-if="user.role_id != 1">
                                        <p class="user-status online" v-if="user.isOnline.original">
                                            онлайн
                                        </p>
                                        <p class="user-status offline" v-else>
                                            не в сети, был(-а)<br>
                                            @{{ user.lastOnline.original }}
                                        </p>
                                    </div>

                                    <p class="text-muted text-sm">
                                        <b>E-mail:</b>
                                        @{{ user.email }}
                                    </p>
                                </div>
                                <div class="col-5 text-center">
                                    <img
                                            src="{{ Admin::getProfilePhoto() }}"
                                            class="img-circle img-fluid mw-100">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <button class="btn btn-primary btn-xs" @click="editShowModal(user.id)">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                                <button class="btn btn-danger btn-xs" @click="deleteUser(user.id)">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="usersModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
             aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">
                            @{{ useAdd ? 'Добавление пользователя' : 'Редактирование пользователя' }}
                            <small class="text-muted" v-if="!useAdd">Дата регистрации: @{{ user.created_at }}</small>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div v-if="useAdd">
                            <v-input
                                    class="form-group"
                                    name="name"
                                :errors="errors"
                                v-model="user.name"
                                placeholder="Имя"
                            ></v-input>
                            <v-input
                                class="form-group"
                                name="login"
                                :errors="errors"
                                v-model="user.login"
                                placeholder="Логин"
                            ></v-input>
                            <div class="form-group">
                                <select2 placeholder="Роль" v-model="user.role">
                                    <option v-for="role of {{ $roles }}"
                                            :value="role.role_id">
                                        @{{ role.role_name }}
                                    </option>
                                </select2>
                            </div>
                            <div class="form-group">
                                <div class="pretty p-default p-curve">
                                    <input type="radio" name="type" value="login" @click="setType('login')">
                                    <div class="state p-success-o">
                                        <label>Запросить пароль при входе</label>
                                    </div>
                                </div>
                                <div class="pretty p-default p-curve">
                                    <input type="radio" name="type" value="set" @click="setType('set')">
                                    <div class="state p-danger-o">
                                        <label>Задать пароль</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" v-if="user.setType === 'login'">
                                <label>Укажите E-mail пользователя:</label>
                                <v-input
                                    class="form-group"
                                    name="email"
                                    :errors="errors"
                                    v-model="user.email"
                                    placeholder="E-mail"
                                >
                                    <small>На этот E-mail придет письмо с ссылкой на форму регистрации</small>
                                </v-input>
                            </div>
                            <div class="form-group" v-if="user.setType === 'set'">
                                <v-input
                                    class="input-group"
                                    name="password"
                                    :errors="errors"
                                    v-model="user.password"
                                    placeholder="Пароль"
                                >
                                    <input type="password" class="form-control" placeholder="Повторите пароль"
                                           v-model="user.password_confirmation">
                                </v-input>
                            </div>
                        </div>
                        <div class="clearfix" v-else>
                            <v-input
                                class="form-group"
                                name="name"
                                v-model="user.name"
                                :readonly="true"
                            ></v-input>
                            <v-input
                                class="form-group"
                                name="login"
                                v-model="user.login"
                                :readonly="true"
                            ></v-input>
                            <div class="form-group">
                                <select2 placeholder="Роль" :value="user.role_id" v-model="user.role_id">
                                    <option v-for="role of {{ $roles }}"
                                            :value="role.role_id">
                                        @{{ role.role_name }}
                                    </option>
                                </select2>
                            </div>
                            <button class="btn btn-link w-100 mb-3"
                                    data-toggle="collapse"
                                    data-target="#userData"
                                    type="button"
                            >
                                Показать другие данные пользователя
                                <i class="fas fa-chevron-down"></i>
                            </button>
                            <div class="collapse" id="userData">
                                <div class="card card-body user-data">
                                    <v-input
                                            v-for="(item, key) of user.user_data"
                                            :name="key"
                                            class="form-group"
                                            v-model="item.value"
                                            :placeholder="item.title"
                                            :readonly="true"
                                    >
                                        <label>@{{ item.title }}</label>
                                    </v-input>
                                </div>
                            </div>
                            <div class="form-group" v-if="showEmailEditInput">
                                <label>Укажите E-mail пользователя:</label>
                                <v-input
                                        class="input-group"
                                        type="email"
                                        name="email"
                                        :errors="errors"
                                        v-model="user.email"
                                        placeholder="E-mail"
                                >
                                    <div class="input-group-prepend">
                                        <button class="btn btn-outline-success" @click="resetPassword">
                                            Продолжить
                                        </button>
                                    </div>
                                </v-input>
                                <small>
                                    На этот E-mail будет отправлено письмо с ссылкой на форму восстановления пароля
                                </small>
                            </div>
                            <div class="form-group" v-else>
                                <a href="#" @click.prevent="resetPassword" class="float-right">
                                    <i><b>Сбросить пароль</b></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" v-if="useAdd" @click="add" class="btn btn-success">Добавить</button>
                        <button type="button" v-else @click="edit" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
