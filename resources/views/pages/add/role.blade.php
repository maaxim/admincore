@extends('admin::template')

@section('content')
    <div class="card card-outline card-success add_role">
        <div class="card-body">
            <form @submit.prevent="add" method="post">
                <v-input
                    class="form-group"
                    name="name"
                    v-model="name"
                    :errors="errors"
                    placeholder="Название"
                ></v-input>

                <div class="form-group">
                    <p-check class="p-icon p-round p-smooth" color="success" v-model="rules.adminPanel.value">
                        <i slot="extra" class="icon fas fa-check"></i>
                        @{{ rules.adminPanel.abbreviation }}
                    </p-check>
                </div>

                <div v-for="item of rules.pages">
                    <div class="form-group">
                        <p-check class="p-switch p-fill" color="success" v-model="item.accept"
                                 :disabled="!rules.adminPanel.value">
                            @{{ item.abbreviation }}
                        </p-check>
                    </div>

                    <transition-group
                                enter-active-class="animate__animated animate__rollIn"
                                leave-active-class="animate__animated animate__hinge">
                        <div class="form-group pl-5" v-for="(param, index) of item.params" :key="index" v-show="item.accept">
                            <p-check class="p-icon p-round p-smooth" color="success" v-model="param.value"
                                     :disabled="!rules.adminPanel.value">
                                <i slot="extra" class="icon fas fa-check"></i>
                                @{{ param.abbreviation }}
                            </p-check>
                        </div>
                    </transition-group>
                </div>

                @csrf
                <button type="submit" class="btn btn-success btn-flat">
                    Добавить <i class="fas fa-plus"></i>
                </button>
            </form>
        </div>
    </div>
@endsection
