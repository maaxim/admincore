@extends('admin::template')

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{ route('AdminAddRole') }}" class="btn btn-success btn-flat">
                Добавить <i class="fas fa-plus"></i>
            </a>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width:1%">#</th>
                    <th>Название</th>
                    <th style="width:7%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{ $role->role_name }}</td>
                        <td>
                            <a href="{{ route('AdminEditRole', ['id'=>$role->role_id]) }}"
                               class="btn btn-primary btn-xs">
                                <i class="fas fa-edit"></i>
                            </a>
                            <button type="button" class="btn btn-danger btn-xs deleteRule"
                                    data-id="{{ $role->role_id }}">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
