@extends('admin::template')

@section('content')
    <div class="card card-outline card-danger" id="report">
        <div class="card-body">
            <p>
                <strong>Ошибка:</strong>
                {{ $error->message }}
            </p>
            <p>
                <strong>Код:</strong>
                {{ $error->code }}
            </p>
            <p>
                <strong>Файл:</strong>
                {{ $error->file }}
            </p>
            <p>
                <strong>Строка:</strong>
                {{ $error->line }}
            </p>

            <div class="d-flex justify-content-center">
                <div>
                    @if($error->fixed)
                        <i class="fas fa-check-circle d-block text-center text-success fa-6x
                            animate__rubberBand animate__delay-1s animate__animated"></i>
                        <h3 class="mt-3">Проблема решена</h3>
                    @else
                        <i class="fas fa-exclamation-triangle d-block text-center text-danger fa-6x
                         animate__animated animate__pulse animate__infinite"></i>
                        <h3 class="mt-3">Проблема не решена</h3>
                    @endif
                </div>
            </div>

            <div class="d-flex justify-content-end">
                <div class="btn-group">
                    @if(!$error->fixed)
                        <button type="button" class="btn btn-outline-success"
                                @click="fixed({{ $error->id }})"
                        >
                            <i class="fas fa-check"></i>
                            Ошибка исправлена
                        </button>
                    @endif
                    <button type="button" class="btn btn-outline-danger"
                            @click="deleteReport({{ $error->id }})"
                    >
                        <i class="fas fa-trash"></i>
                        Удалить
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
