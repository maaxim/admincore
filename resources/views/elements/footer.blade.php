<footer class="main-footer">
    <div class="float-right d-none d-sm-inline">
        Разработчик: <a href="https://vk.com/klimenkom1" target="_blank" class="pr-2">Максим</a>
    </div>
    <strong>&copy; 2020 - {{ date('Y') }}</strong>
    <span class="pl-0 pl-sm-3">Версия: <strong>{{ Admin::getVersion() }}</strong></span>
</footer>
