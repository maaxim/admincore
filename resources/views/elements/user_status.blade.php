<p class="user-status {{ (new \maaxim\admincore\Http\Controllers\Auth\Profile(new \Illuminate\Http\Request()))->isOnline() ? 'online' : 'offline' }}">
    @if((new \maaxim\admincore\Http\Controllers\Auth\Profile(new \Illuminate\Http\Request()))->isOnline())
        онлайн
    @else
        не в сети, был(-а)<br>
        {{ (new \maaxim\admincore\Http\Controllers\Auth\Profile(new \Illuminate\Http\Request()))->getLastOnline() }}
    @endif
</p>
