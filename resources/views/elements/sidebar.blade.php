<aside class="main-sidebar elevation-4
    {{ Admin::getUserData('customize.bg_main_link', 'sidebar-dark-primary') }}
{{ Admin::getUserData('customize.off_auto_open_style', false) ? ' sidebar-no-expand' : '' }}
    ">
    <a href="/" target="_blank"
       class="brand-link {{ Admin::getUserData('customize.bg_logo', 'navbar-dark') }}">
        <img src="{{ Admin::getResource('/images/AdminLogo.png') }}"
             class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Admin Panel</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image d-flex flex-nowrap align-items-center">
                <img src="{{ Admin::getProfilePhoto() }}"
                     class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('AdminProfile') }}" class="d-block">{{ Auth::user()->name }}</a>
                @include('admin::elements.user_status')
            </div>
        </div>

        <div class="form-inline mb-3">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Поиск по навигации"
                       aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
            <div class="sidebar-search-results">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <div class="search-title">
                            Ничего не найдено
                        </div>
                        <div class="search-path">
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column {{ Admin::getSidebarNavClasses() }}"
                data-widget="treeview" role="menu" data-accordion="false">
                {!! Link::render() !!}
            </ul>
        </nav>
    </div>
</aside>
