<nav
        class="main-header navbar navbar-expand {{ Admin::getUserData('customize.bg_top_nav', 'navbar-dark') }}
        {{ Admin::getUserData('customize.header_no_border', false) ? ' border-bottom-0' : '' }}
                ">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <button class="nav-link" data-widget="clear_cache" role="button"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Очистить кеш"
            ><i class="fas fa-broom"></i></button>
        </li>
        <li class="nav-item">
            <button class="nav-link" data-widget="optimize" role="button"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Оптимизировать приложение"
            ><i class="fas fa-microchip"></i></button>
        </li>

        @if(config('admin.errors_report') == true)
            <li class="nav-item dropdown report-exception-dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fas fa-bug"></i>
                    <span class="badge badge-danger navbar-badge">
                        {{ \maaxim\admincore\ErrorReport::getNewErrorsCount() }}
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    @foreach(\maaxim\admincore\ErrorReport::getErrors() as $item)
                        <a href="{{ route('AdminReport', ['id'=>$item->id]) }}" class="dropdown-item">
                            <div class="media">
                                <i class="fas fa-exclamation-triangle img-size-50 mr-3 img-circle"></i>
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        ErrorException #{{ $item->id }}
                                    </h3>
                                    <p class="text-sm">{{ substr($item->message, 0, 25) }}...</p>
                                    <p class="text-sm text-muted">
                                        <i class="far fa-clock mr-1"></i> {{ $item->created_at }}
                                    </p>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                    @endforeach
                    <a href="{{ route('AdminReports') }}" class="dropdown-item dropdown-footer">
                        Посмотреть все
                    </a>
                </div>
            </li>
        @endif

        @foreach(Widget::getWidgetsHeaderDropdown() as $item)
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="{{ $item->getIcon() }}"></i>
                    @if($item->getBadge() != null)
                        <span class="badge badge-{{ $item->getBadge()->getColor() }} navbar-badge">
                            {{ $item->getBadge()->getVal() }}
                        </span>
                    @endif
                    {{ $item->getTitle() }}
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    {!! $item->getView() !!}
                </div>
            </li>
        @endforeach

        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="{{ Admin::getProfilePhoto() }}"
                     class="user-image img-circle elevation-2">
                <span class="d-none d-md-inline">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0;">
                <li class="user-header bg-primary">
                    <img src="{{ Admin::getProfilePhoto() }}"
                         class="img-circle elevation-2" alt="User Image">
                    <p>
                        {{ Auth::user()->name }}
                        - {{ \maaxim\admincore\AdminRole::find(Auth::user()->role_id)->role_name }}
                    </p>
                </li>
                <li class="user-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('AdminLockscreenLock') }}" method="post">
                                @csrf
                                <button type="submit" class="btn mr-auto ml-auto d-block">Заблокировать экран</button>
                            </form>
                        </div>
                    </div>
                </li>
                <li class="user-footer">
                    <form action="{{ route('AdminLogout') }}" method="post">
                        <a href="{{ route('AdminProfile') }}" class="btn btn-default btn-flat">Профиль</a>
                        @csrf
                        <button type="submit" class="btn btn-default btn-flat float-right">Выйти</button>
                    </form>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
    </ul>
</nav>
