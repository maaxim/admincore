@component('mail::message')
<p>Вы получили это письмо, потому что мы получили запрос на сброс пароля для вашей учетной записи.</p>

@component('mail::button', ['url' => route('AdminResetPassword', ['token' => $token, 'email' => $email])])
Сбросить пароль
@endcomponent

<p>Если вы не запрашивали сброс пароля, никаких дальнейших действий не требуется.</p>

<hr>

<small>Если у вас возникли проблемы с нажатием кнопки «Продолжить регистрацию», скопируйте и вставьте приведенный ниже
URL-адрес в ваш веб-браузер:
<a href="{{ route('AdminResetPassword', ['token' => $token, 'email' => $email]) }}">
{{ route('AdminResetPassword', ['token' => $token, 'email' => $email]) }}
</a>
</small>
@endcomponent
