@component('mail::message')
<p>Чтобы продолжить регистрацию в AdminPanel перейдите по ссылке ниже.</p>

@component('mail::button', ['url' => route('firstLogin', ['key' => $key, 'id' => $id])])
Продолжить регистрацию
@endcomponent

<small>Если у вас возникли проблемы с нажатием кнопки «Продолжить регистрацию», скопируйте и вставьте приведенный ниже
URL-адрес в ваш веб-браузер:
<a href="{{ route('firstLogin', ['key' => $key, 'id' => $id]) }}">
{{ route('firstLogin', ['key' => $key, 'id' => $id]) }}
</a>
</small>
@endcomponent
