@component('mail::message')
<p>На сайте что-то пошло не так!</p>

<p>
<strong>Ошибка:</strong>
{{ $e->getMessage() }}
</p>

<p>
<strong>Файл:</strong>
{{ $e->getFile() }}
</p>

<p>
<strong>Строка:</strong>
{{ $e->getLine() }}
</p>
@endcomponent
