@extends('admin::template')

@section('content')
    <div class="row" id="profile">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                             src="{{ Admin::getProfilePhoto() }}">
                    </div>
                    <h3 class="profile-username text-center">@{{ name }}</h3>
                    <div class="mb-1">
                        @include('admin::elements.user_status')
                    </div>
                    <p class="text-muted text-center">
                        {{ \maaxim\admincore\AdminRole::find(Auth::user()->role_id)->role_name }}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" href="#settings" data-toggle="tab">Настройки</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#password" data-toggle="tab">Изменить пароль</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#personalization" data-toggle="tab">Персонализация</a>
                        </li>
                        @foreach(Widget::getWidgetsProfilePanel() as $item)
                            <li class="nav-item">
                                <a class="nav-link" href="#{{ $item->getKey() }}" data-toggle="tab">
                                    {{ $item->getTitle() }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <form @submit.prevent="save" class="form-horizontal">
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Имя</label>
                                    <v-input
                                            class="col-sm-10"
                                            :errors="errors"
                                            type="text"
                                            name="name"
                                            id="inputName"
                                            placeholder="Имя"
                                            v-model="name"
                                    ></v-input>
                                </div>
                                <div class="form-group row">
                                    <label for="inputLogin" class="col-sm-2 col-form-label">Логин</label>
                                    <v-input
                                            class="col-sm-10"
                                            :errors="errors"
                                            type="text"
                                            name="login"
                                            id="inputLogin"
                                            placeholder="Логин"
                                            v-model="login"
                                    ></v-input>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-2 col-form-label">E-mail</label>
                                    <v-input
                                            class="col-sm-10"
                                            :errors="errors"
                                            name="email"
                                            type="email"
                                            id="inputEmail"
                                            placeholder="E-mail"
                                            v-model="email"
                                    ></v-input>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Фото</label>
                                    <div class="col-sm-10">
                                        <input-file
                                                v-model="photo"
                                                :show-files-size="true"
                                                accept="image/*"
                                        ></input-file>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10 clearfix">
                                        <button type="submit" class="btn btn-success float-right">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="password">
                            <form @submit.prevent="resetPassword">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Новый пароль</label>
                                    <v-input
                                        class="col-sm-10"
                                        :errors="errors"
                                        type="password"
                                        name="password"
                                        id="inputPassword"
                                        placeholder="Новый пароль"
                                        v-model="password"
                                    ></v-input>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPasswordСonfirmation" class="col-sm-2 col-form-label">Повторите пароль</label>
                                    <v-input
                                        class="col-sm-10"
                                        :errors="errors"
                                        type="password"
                                        name="password_confirmation"
                                        id="inputPasswordСonfirmation"
                                        placeholder="Повторите пароль"
                                        v-model="password_confirmation"
                                    ></v-input>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10 clearfix">
                                        <button type="submit" class="btn btn-success float-right">Сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="personalization">
                            <form @submit.prevent="personalizationSave">
                                <label>Тема AdminPanel:</label>
                                <div class="form-group customize-themes">
                                    <span class="theme-dark-button" :class="{'active': themeDark}"
                                          @click="themeChange"></span>
                                    <span class="theme-light-button" :class="{'active': !themeDark}"
                                          @click="themeChange"></span>
                                </div>

                                <div class="form-group">
                                    <label>Автоматическая блокировка экрана через:</label>
                                    <select2
                                            :options="lockScreenOptions"
                                            :value="lockScreenValue"
                                            v-model="lockScreenValue"
                                    ></select2>
                                </div>

                                <button type="submit" class="btn btn-success float-right">
                                    <i class="fas fa-check"></i>
                                    Сохранить
                                </button>
                            </form>
                        </div>
                        @foreach(Widget::getWidgetsProfilePanel() as $item)
                            <div class="tab-pane" id="{{ $item->getKey() }}">
                                {!! $item->getView() !!}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
