<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Сброс пароля | Admin Panel</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ Admin::getResource('images/AdminLTELogo.png') }}">
    <script src="{{ Admin::getResource('js/app.js') }}"></script>
    <script src="{{ Admin::getResource('js/vendor.js') }}"></script>
    <script src="{{ Admin::getResource('js/manifest.js') }}"></script>
    <link rel="stylesheet" href="{{ Admin::getResource('css/app.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('AdminHome') }}"><b>Admin</b> Panel</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Сброс пароля</p>
            <form action="{{ route('AdminResetPassword', ['token' => $token, 'email' => $email]) }}" method="post">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') ?? $email }}" placeholder="E-mail"
                           required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                           placeholder="Пароль" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                           name="password_confirmation" placeholder="Повторите пароль" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="float-right">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Сбросить пароль</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
