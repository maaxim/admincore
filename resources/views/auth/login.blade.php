<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Авторизация | Admin Panel</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ Admin::getResource('images/AdminLTELogo.png') }}">
    <script src="{{ Admin::getResource('js/app.js') }}"></script>
    <script src="{{ Admin::getResource('js/vendor.js') }}"></script>
    <script src="{{ Admin::getResource('js/manifest.js') }}"></script>
    <link rel="stylesheet" href="{{ Admin::getResource('css/app.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('AdminHome') }}"><b>Admin</b> Panel</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Авторизация</p>
            <form action="{{ route('AdminLogin') }}" method="post">
                @error('login')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <input type="text" class="form-control @error('login') is-invalid @enderror" name="login"
                           value="{{ old('login') }}" placeholder="Логин"
                           required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control @error('login') is-invalid @enderror" name="password"
                           placeholder="Пароль" required>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="pretty p-icon p-curve p-tada">
                            <input type="checkbox" name="remember">
                            <div class="state p-success-o">
                                <i class="icon fas fa-check"></i>
                                <label>Запомнить меня</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
