<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>Блокировка экрана | AdminPanel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Admin::renderResources() !!}
</head>
<body
        class="hold-transition lockscreen{{ Admin::isThemeDark() ? ' dark-mode swal-dark' : '' }}">
<div class="lockscreen-wrapper" id="lockscreen">
    <div class="lockscreen-logo">
        <span><b>Admin</b>Panel</span>
    </div>

    <div class="lockscreen-name">{{ Auth::user()->name }}</div>

    <div class="lockscreen-item">
        <div class="lockscreen-image">
            <img src="{{ Admin::getProfilePhoto() }}">
        </div>
        <form @submit.prevent="unlock" class="lockscreen-credentials">
            <v-input
                    class="input-group"
                    :errors="errors"
                    type="password"
                    placeholder="Пароль"
                    name="password"
                    v-model="password"
            >
                <div class="input-group-append">
                    <button type="submit" class="btn"><i class="fas fa-arrow-right text-muted"></i></button>
                </div>
            </v-input>
        </form>
    </div>
</div>
</body>
</html>
