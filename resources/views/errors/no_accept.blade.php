@extends('admin::template')

@section('content')
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-warning">401</h2>

            <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i> Доступ запрещён</h3>

                <p>
                    Для вашей роли "<b>{{ $role_name }}</b>" установлено ограничение на эту страницу
                </p>
            </div>
        </div>
    </section>
@endsection
