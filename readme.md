# AdminCore

![Скрин](media/photo.png)

* Авторизация пользователя только с доступом в AdminPanel
* Тонкая настройка роли пользователя можно ограничить доступ к каждой отдельной функции
* ErrorReport для всех ошибок которые возникли во время работы всего сайта
* Оптимизация и очистка кеша всего Web приложения
* Доступна темная и светлая тема и ещё порядка 15 настроек для изменения внешнего вида AdminPanel

Laravel 8.x|9.x

**Require Packages:**

* [laravel/ui](https://github.com/laravel/ui/tree/3.x)
* [curl/curl](https://github.com/curl/curl)
* [fortawesome5-free](https://fontawesome.com/v5/search?o=r&m=free)
* [sweetalert2](https://sweetalert2.github.io/)
* [bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [ckeditor 4](https://ckeditor.com/docs/ckeditor4/latest/guide/dev_installation.html)
* [ckeditor4-vue](https://www.npmjs.com/package/ckeditor4-vue)
* [datatables.net-bs4](https://www.npmjs.com/package/datatables.net-bs4)
* [lodash](https://lodash.com/docs/4.17.15)
* [pretty-checkbox-vue](https://www.npmjs.com/package/pretty-checkbox-vue)
* [select2](https://select2.org/)
* [vue 2](https://ru.vuejs.org/v2/guide/)

## Установка

Для установки нужно добавить репозиторий в `composer.json`.

```json
{
  "repositories": [
    {
      "type": "git",
      "url": "https://maaxim@bitbucket.org/maaxim/admincore.git"
    }
  ]
}
```

```json
{
  "require": {
    "maaxim/admincore": "^2.0.0"
  }
}
```

И выполнить команду `composer install`. После установки репозитория надо выполнить миграции `php artisan migrate`.
Теперь надо выполнить первичную настройку AdminPanel: `php artisan admin:install`. После вы можете войти в AdminPanel по
пути
http://your-web-site/admin/login

Логин - user. Пароль - 123

## Иерархия

**app/admin/links.php** - файл для определения ссылок в панели навигации AdminPanel

**app/admin/rules.php** - файл для определения правил доступа AdminPanel

**app/Http/Controllers/Admin/** - Путь для контролеров AdminPanel

```php
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Page extends Controller
{
    protected Request $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function page()
    {
        return view('admin.page', [
            'title' => 'Заголовок страницы'
        ]);
    }
}

```

**config/admin.php**

```php
return [
    'links' => [
        app_path() . '/admin/links.php'
    ],
    'rules' => [
        app_path() . '/admin/rules.php'
    ],

    // основной путь где лежат css/js/media файлы AdminPanel
    'resource_path' => 'admin_resources',
    'resources' => [
        'css' => [
            // кастомные css файлы
            // например '/css/admin.css'
            // полный путь ../public/{resource_path}/css/admin.css
        ],
        'js' => [
            // кастомные js файлы
        ]
    ],

    // роли которые запрещено редактировать(id, по умолчанию Администратор и Пользователь)
    'roles_without_interaction' => [1, 2],

    // создавать ли отчет об ошибках
    'errors_report' => false,
    // отправлять ли уведомление об ошибке
    'send_email_report' => false,
    // куда отправлять
    'emails' => 'example@domain.com',

    // настройка для работы vue-devtool
    'vue-devtool' => false,
    'vue-devtool-script' => '<script src="http://localhost:8098"></script>',

    // показывать загрузчик или нет
    'preloader' => false,
    // хлебные крошки
    'breadcrumbs' => true
];
```

**resources/views/admin** - путь для blade шаблонов

```php
@extends('admin::template')

@section('content')
    //
@endsection
```

**routes/admin.php** - файл роутов AdminPanel

## Как настроить отчет об ошибках

Надо настроить под себя файл конфига `admin.php`. Здесь пару вариантов, создавать репорт только внутренний или же с
отправкой уведомления на почту.

```php
return [
    // ..
    'errors_report' => true, // включаем
    'send_email_report' => true, // тоже включаем если надо
    'emails' => 'programmer@domain.com', // и прописываем почту
    // ..
];
```

Помимо почты вы можете добавить своих [слушателей события](https://laravel.com/docs/9.x/events#main-content). Обработчик
содержит в себе только один параметр `public $_e` типа `Throwable`.

**App/Providers/EventServiceProvider.php**

```php
protected $listen = [
    // ..
    maaxim\admincore\Events\ErrorReport::class => [
        App\Listeners\YourListener::class,
    ]
    // ..
];
```

После всех настроек надо запустить слушателя ошибок и вызов обработчика событий.

**App/Exceptions/Handler.php**

```php
public function register()
{
    $this->reportable(function (Throwable $e) {
        event(new maaxim\admincore\Events\ErrorReport($e));
    });
}
```

## Как создать страницу и добавить её в панель навигации

Ну давайте сначала скажу что AdminCore работает по принципу базового Laravel только с надстройками.

Создаём стандартный Laravel Controller, допустим обзовем его news(новости)
`php artisan make:controller Admin/News` и добавим в него функцию `page`. Далее соответственно нам нужен вид, создаем
его, я обычно делаю по пути
**resources/views/admin** и создаем файл `news.blade.php`. Основная разметка файла:

```php
@extends('admin::template')

@section('content')
    // здесь какое-то содержание
@endsection
```

Теперь вызываем этот вид в контроллере `News`

```php
public function page()
{
    return view('admin.news', [
        'title' => 'Новости' // Заголовок страницы
    ]);
}
```

Отлично, пропишем роут _routes/admin.php_

```php
Route::get('/news', 'Admin/News@page')->name('AdminNews');
```

Отлично! Теперь у нас есть это г... Теперь добавим страницу в панель навигации. Для этого нам нужен файл _
app/admin/links.php_

```php
Link::add()
    ->name('Новости')
    ->route('AdminNews');
```

Ну собственно все... Дальше уже ваша фантазия...) База есть. Но давайте рассмотрим немного побольше о возможностях **
AdminCore**.

## Фасад `Link` файл **links.php**

* `function add():` объявляет создание нового элемента или группы элементов
* `function name(string $name):` присваивает "имя" объекта(ссылки)
* `function route(string $route):` присваивает роут объекту - прописывается именно имя роута
* `function icon(string $icon):` присваивает иконку для ссылки, используются иконки
  [fortawesome5-free](https://fontawesome.com/v5/search?o=r&m=free)
* `function badge(Closure|string|int $value, string $color): ` создает badge(уведомление на ссылке). Closure должен
  вернуть строковое либо цифровое значение!
* `function active(string|array $routes)` - активная ссылка на других страницах, допустим у нас есть страница
  редактирования новости, при переходе на редактирование ссылка Новости потеряет свое выделение как "активной". Но эта
  функция исправит это если прописать имя роута страницы редактирования.

```php
Link::add()
    ->name('Главная')
    ->route('AdminHome')
    ->icon('fas fa-tachometer-alt');
```

Так же можно делать и группы ссылок.

```php
Link::add()
    ->name('Акции')
    ->group(function(maaxim\admincore\Classes\Links\BuildLink $link) {
        $link->add()
            ->name('Хит продаж');

        $link->add()
            ->name('Новинки');
    });
```

## Фасад `Role` файл **rules.php**

Этот фасад отвечает за работу правил ролей т.е. мы в нем определяем группы правил, а после используем в нужном нам
контроллере.

Самый простой способ использования:

```php
Role::setPageGroup('news', 'Новости', (new maaxim\admincore\Classes\Roles\PageRule()));
```

Здесь мы создали новую группу правил для страницы Новости.

* Первое значение это название группы, грубо говоря её id. Значение должно быть уникальным, иначе будет просто
  перезапись группы.
* Второе значение это аббревиатура, то же название, но для вывода на страницу пользователю.
* Третьим параметром мы передаём новый класс PageRule, в котором по умолчанию задано 3 правила.
  * Добавление
  * Редактирование
  * Удаление

Каждое из этих правил определяет разрешено ли пользователю с данной ролью, добавлять, редактировать или удалять записи.
Правила можно добавлять уже к этому списку

```php
Role::setPageGroup(
    'news', 'Новости',
    (new maaxim\admincore\Classes\Roles\PageRule())
        ->setCustomRule('public', false, 'Публикация из черновика')
);
```

Так мы добавили новое правило, можно ли пользователю с этой ролью публиковать новость из черновика.

* Первый параметр это название группы
* Второе значение по умолчанию **(bool)**
* Третье аббревиатура Так же правила можно полностью переопределить, убрать правила прописанные по умолчанию и написать
  свои.

```php
Role::setPageGroup(
    'news', 'Новости',
    (new maaxim\admincore\Classes\Roles\PageRule())
        ->setCustomParams([
            'public' => [
                'value' => false,
                'abbreviation' => 'Публикация из черновика'
            ],
            // ..
        ])
);
```

Так мы поняли как создавать группы правил. Как применить? Обработка этих правил проходит через обычный middleware(пока
что). Каждый элемент привязывается к роуту, допустим у нас есть группа news и правило add, чтобы определить в каком
месте применять правило осуществляется привязка роут -> правило. Так же нужно наследовать не базовый LaravelController,
а
`maaxim\admincore\Http\Controllers\AdminController`.

```php
public function __construct(\Illuminate\Http\Request $request)
{
    // Здесь мы привязываем правило к роуту, обязательно прописывать группа.правило
    // Вторым параметром передается имя основной рабочей группы правил
    $this->setRulePage([
        'AdminNewsAdd' => 'news.add',
        'AdminNewsEdit' => 'news.edit',
        'AdminNewsDelete' => 'news.delete',
    ], 'news');

    // Здесь уже выполняет проверка на доступ
    // Первым параметром мы передаем стандартный Request
    // Вторым параметром передаем массив имен функций которые не будут проверяться допустим для Json запросов с
    // какими-либо возвращаемыми данными
    $this->checkRules($request, ['function-name']);
}
```

После все работает в автоматическом режиме.

## Фасад `Widget`

Этот фасад создаёт виджеты. На данный момент доступно только 2 виджета.

* DropdownMenu в Sidebar(в верхней части)
* Панель в Профиле

![DropdownMenu](media/dropdown_menu.png)
![Панель в Профиле](media/profile_panel.png)

Оба виджета создаются довольно просто. Для определения виджетов можно создать ServiceProvider(по умолчанию где их
прописывать, не задано).

**DropdownMenu**

```php
Widget::makeHeaderDropdown()
    ->setTitle('Что-то') // заголовок
    ->setIcon('fas fa-check') // иконка
    ->setBadge('success', function () {
        return 5;
    }) // уведомление на виджет
    ->setView('test'); // обычный вид, создаете в папке видов и указываете его, это и будет содержание dropdown
```

**Панель в Профиле**

```php
Widget::makeProfilePanel()
    ->setTitle('Test') // заголовок
    ->setView('test'); // вид
```

На этом пока все. :)
