<?php

use maaxim\admincore\AdminRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use maaxim\admincore\Facades\Role;
use maaxim\admincore\Classes\Roles\PageRule;

class AdminRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'role_name' => 'Пользователь',
                'rules' => json_encode(Role::setAdminPanel(false)->getRules()),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'role_name' => 'Администратор',
                'rules' => json_encode(
                    Role::setAdminPanel(true)
                        ->setPageGroup('users', 'Пользователи',
                            (new PageRule())
                                ->setAdd(true)
                                ->setEdit(true)
                                ->setDelete(true)
                                ->setCustomRule('reset', true, 'Сброс паролей'),
                            true
                        )
                        ->setPageGroup('roles', 'Роли',
                            (new PageRule())
                                ->setAdd(true)
                                ->setEdit(true)
                                ->setDelete(true),
                            true
                        )
                        ->getRules()
                ),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'role_name' => 'Менеджер',
                'rules' => json_encode(
                    Role::setAdminPanel(true)
                        ->setPageGroup('users', 'Пользователи',
                            (new PageRule())
                                ->setAdd(true)
                                ->setCustomRule('reset', true, 'Сброс паролей'),
                            true
                        )
                        ->setPageGroup('roles', 'Роли', (new PageRule()))
                        ->getRules()
                ),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ];

        DB::table((new AdminRole())->getTable())->insert($roles);
    }
}
