<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new \App\Models\User();
        $new->name = 'User';
        $new->login = 'user';
        $new->email = 'user@gmail.com';
        $new->password = Hash::make('123');
        $new->role_id = 2;
        $new->save();
    }
}
