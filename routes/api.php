<?php

// prefix: admin/api

Route::post('user/isOnline', 'Auth\Profile@isOnline');
Route::post('user/getLastOnline', 'Auth\Profile@getLastOnline');
