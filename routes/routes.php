<?php

Route::middleware('isAdmin')->group(function () {
    Route::post('logout', 'Auth\LoginController@logout')->name('AdminLogout');
    Route::post('cache/clear', 'ApplicationCache@cacheClear');
    Route::post('cache/optimize', 'ApplicationCache@optimize');

    Route::prefix('profile')->group(function () {
        Route::get('/', 'Auth\Profile@page')->name('AdminProfile');
        Route::post('user', 'Auth\Profile@getUserData');
        Route::post('save', 'Auth\Profile@save');
        Route::post('personalization', 'Auth\Profile@personalization');
        Route::post('customize', 'Auth\Profile@customize');
        Route::post('customize/set/dark', 'Auth\Profile@setDarkTheme');
        Route::post('check/password', 'Auth\Profile@checkPassword');
        Route::post('reset/password', 'Auth\Profile@resetPassword');
        Route::post('get/lockScreenTimeout', 'Auth\Profile@getLockScreenTimeout');
    });

    Route::get('lockscreen', 'Lockscreen@page')->name('AdminLockscreen');
    Route::post('lockscreen/unlock', 'Lockscreen@unlock')->name('AdminLockscreenUnlock');
    Route::post('lockscreen/lock', 'Lockscreen@lock')->name('AdminLockscreenLock');

    Route::prefix('users')->group(function () {
        Route::get('/', 'Users@page')->name('AdminUsers');
        Route::post('get', 'Users@getUsers');
        Route::post('getUserRegisterLink', 'Users@getUserRegisterLink');
        Route::post('add', 'Users@addUser')->name('AdminUsersAdd');
        Route::post('edit', 'Users@editUser')->name('AdminUsersEdit');
        Route::post('delete', 'Users@deleteUser')->name('AdminUsersDelete');
        Route::post('reset_password', 'Users@resetPassword')->name('AdminUsersResetPassword');

        Route::prefix('roles')->group(function () {
            Route::get('/', 'Roles@page')->name('AdminRoles');
            Route::post('/delete', 'Roles@delete')->name('AdminRoleDelete');

            Route::post('/get/rules', 'Roles@getRules');
            Route::post('/get/name', 'Roles@getRoleName');

            Route::get('add', 'Roles@addPage')->name('AdminAddRole');
            Route::post('add', 'Roles@add');

            Route::get('edit', 'Roles@editPage')->name('AdminEditRole');
            Route::post('edit', 'Roles@edit');
        });
    });

    Route::get('reports', 'Reports@page')->name('AdminReports');
    Route::get('reports/{id}', 'Reports@report')->name('AdminReport');
    Route::post('report/fixed', 'Reports@fixed')->name('AdminReportFix');
    Route::post('report/delete', 'Reports@delete')->name('AdminReportDelete');
    Route::post('report/clear', 'Reports@clearTable');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('AdminLogin');
Route::post('login', 'Auth\LoginController@login');

Route::get('login/{key}/{id}', 'Auth\LoginController@firstLogin')->name('firstLogin');
Route::post('login/{key}/{id}', 'Auth\LoginController@firstLoginSave');

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@page')->name('AdminResetPassword');
Route::post('password/reset/{token}', 'Auth\ResetPasswordController@reset');
