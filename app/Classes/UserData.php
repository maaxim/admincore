<?php

namespace maaxim\admincore\Classes;

use stdClass;
use Illuminate\Support\Arr;
use maaxim\admincore\AdminData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

trait UserData
{
    protected static $_model = null;

    protected static function setModel($id = null)
    {
        if ($id == null) {
            self::$_model = Cache::remember('user_data_'.Auth::id(), 86400, function () {
                return AdminData::where('user_id', Auth::id())->get()->toArray();
            });
        } else {
            self::$_model = Cache::remember('user_data_'.$id, 86400, function () use ($id) {
                return AdminData::where('user_id', $id)->get()->toArray();
            });
        }
    }

    /**
     * Извлечение данных пользователя из таблицы 'admin_data'
     * Если записи на пользователя нет, то создаём запись и возвращаем объект модели(если не указан key)
     *
     * @param string $key
     * @param string|int|boolean|array $default
     * @return AdminData|stdClass|string|int|boolean|array
     */
    public static function getUserData(string $key = '', $default = '')
    {
        if (self::$_model == null) {
            self::setModel();
        }

        $admin_data = self::$_model;

        if (!empty($admin_data)) {
            $admin_data = Cache::remember('user_data_model_' . Auth::id(), 86400, function () use ($admin_data) {
                return AdminData::find($admin_data[0]['data_id']);
            });
        } else {
            Cache::forget('user_data_'.Auth::id());

            $admin_data = new AdminData();
            $admin_data->user_id = Auth::id();
            $admin_data->data = json_encode([]);
            $admin_data->save();
        }

        if ($key !== '') {
            $key = explode('.', $key);

            if (count($key) == 1) {
                $key = $key[0];
                $admin_data = json_decode($admin_data->data)->$key ?? $default;
            } else {
                $result = json_decode($admin_data->data);
                $keysExists = true;

                foreach ($key as $item) {
                    if (Arr::has((array) $result, $item)) {
                        $result = $result->$item;
                    } else {
                        $admin_data = $default;
                        $keysExists = false;
                        break;
                    }
                }

                if ($keysExists) {
                    $admin_data = $result;
                }
            }
        }

        self::$_model = null;
        return $admin_data;
    }

    public static function getDataByUserId($id, $key = '', $default = '')
    {
        self::setModel($id);
        $data = self::getUserData($key, $default);
        self::$_model = null;

        return $data;
    }

    /**
     * @param array $data
     */
    public static function setUserData(array $data)
    {
        $admin_data = self::getUserData();
        $admin_datum = (array) json_decode($admin_data->data);

        foreach ($data as $key => $item) {
            $admin_datum[$key] = $item;
        }

        $admin_data->data = json_encode($admin_datum);
        $admin_data->save();

        Cache::forget('user_data_'.Auth::id());
        Cache::forget('user_data_model_'.Auth::id());
    }
}
