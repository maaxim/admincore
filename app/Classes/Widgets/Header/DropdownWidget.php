<?php

namespace maaxim\admincore\Classes\Widgets\Header;

trait DropdownWidget
{
    protected array $_widgets = [];

    /**
     * @return Dropdown
     */
    public function makeHeaderDropdown(): Dropdown
    {
        $this->_widgets[] = new Dropdown();
        return $this->_widgets[count($this->_widgets) - 1];
    }

    /**
     * @return array
     */
    public function getWidgetsHeaderDropdown(): array
    {
        return $this->_widgets;
    }
}
