<?php

namespace maaxim\admincore\Classes\Widgets\Header;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Contracts\View\View;
use maaxim\admincore\Classes\Widgets\Widget;
use Illuminate\Contracts\View\Factory as ViewFactory;

class Dropdown implements Widget
{
    protected ?string $_title = null;
    protected ?Badge $_badge = null;
    protected ?string $_icon = null;
    protected string $_key;
    protected $_view = null;

    public function __construct()
    {
        $this->_key = Str::random(24);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->_title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): Dropdown
    {
        $this->_title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->_key;
    }

    /**
     * @return View|ViewFactory
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @param string $view
     * @param array $data
     * @return $this
     */
    public function setView(string $view, array $data = []): Dropdown
    {
        $this->_view = view($view, $data);
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->_icon;
    }

    /**
     * @param mixed $icon
     * @return Dropdown
     */
    public function setIcon($icon): Dropdown
    {
        $this->_icon = $icon;
        return $this;
    }

    /**
     * @return Badge|null
     */
    public function getBadge(): ?Badge
    {
        return $this->_badge;
    }

    /**
     * @param string $color
     * @param Closure|null $callback
     * @return $this
     */
    public function setBadge(string $color, Closure $callback = null): Dropdown
    {
        $this->_badge = (new Badge())
            ->setColor($color)
            ->setVal($callback());
        return $this;
    }
}
