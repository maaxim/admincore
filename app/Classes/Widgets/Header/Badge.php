<?php

namespace maaxim\admincore\Classes\Widgets\Header;

class Badge
{
    protected string $_color;
    protected string $_val;

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->_color;
    }

    /**
     * @param string $color
     * @return Badge
     */
    public function setColor(string $color): Badge
    {
        $this->_color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getVal(): string
    {
        return $this->_val;
    }

    /**
     * @param string $val
     * @return Badge
     */
    public function setVal(string $val): Badge
    {
        $this->_val = $val;
        return $this;
    }
}
