<?php

namespace maaxim\admincore\Classes\Widgets;

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;

interface Widget
{
    public function setTitle(string $title);

    public function getTitle();

    public function getKey();

    public function setView(string $view, array $data = []);

    public function getView();
}
