<?php

namespace maaxim\admincore\Classes\Widgets\Profile;

trait PanelWidget
{
    protected array $_profile_panel_widgets = [];

    /**
     * @return Panel
     */
    public function makeProfilePanel(): Panel
    {
        $this->_profile_panel_widgets[] = new Panel();
        return $this->_profile_panel_widgets[count($this->_profile_panel_widgets) - 1];
    }

    /**
     * @return array
     */
    public function getWidgetsProfilePanel(): array
    {
        return $this->_profile_panel_widgets;
    }
}
