<?php

namespace maaxim\admincore\Classes\Widgets\Profile;

use Illuminate\Support\Str;
use Illuminate\Contracts\View\View;
use maaxim\admincore\Classes\Widgets\Widget;
use Illuminate\Contracts\View\Factory as ViewFactory;

class Panel implements Widget
{
    protected $_title;
    protected $_view;
    protected $_key;

    public function __construct()
    {
        $this->_key = Str::random(24);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param string $title
     * @return Panel
     */
    public function setTitle(string $title): Panel
    {
        $this->_title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->_key;
    }

    /**
     * @return View|ViewFactory
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @param string $view
     * @param array $data
     * @return Panel
     */
    public function setView(string $view, array $data = []): Panel
    {
        $this->_view = view($view, $data);
        return $this;
    }
}
