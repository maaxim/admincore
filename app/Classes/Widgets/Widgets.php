<?php

namespace maaxim\admincore\Classes\Widgets;

use maaxim\admincore\Classes\Widgets\Profile\PanelWidget;
use maaxim\admincore\Classes\Widgets\Header\DropdownWidget;

class Widgets
{
    use PanelWidget, DropdownWidget;
}
