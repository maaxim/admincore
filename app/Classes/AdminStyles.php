<?php


namespace maaxim\admincore\Classes;

trait AdminStyles
{
    protected static array $_resources = [];

    public static function setResourceCss($path)
    {
        if (file_exists(public_path($path)))
        {
            self::$_resources[] = self::getCssTemplate($path);
        }
    }

    public static function setResourceJs($path)
    {
        if (file_exists(public_path($path)))
        {
            self::$_resources[] = self::getJsTemplate($path);
        }
    }

    protected static function getCssTemplate($path)
    {
        $v = fileatime(public_path($path));
        $path = asset($path);

        return "<link rel='stylesheet' href='{$path}?v={$v}'>";
    }

    protected static function getJsTemplate($path): string
    {
        $v = fileatime(public_path($path));
        $path = asset($path);

        return "<script src='{$path}?v={$v}'></script>";
    }

    public static function getResources(): array
    {
        return self::$_resources;
    }

    public function renderResources(): string
    {
        $str = "";

        foreach (self::getResources() as $resource) {
            $str .= "$resource\n";
        }

        return $str;
    }
}
