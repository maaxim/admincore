<?php

namespace maaxim\admincore\Classes;

use stdClass;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class AdminCore
{
    use UserData, AdminStyles;

    protected static string $_profileImagesPath = 'media/admin/profile';
    protected static array $_resourcePaths = [];
    protected static array $_languages = [];
    protected static string $version;

    public function __construct()
    {
        self::$version = json_decode(file_get_contents(__DIR__ . '/../../composer.json'))->version;
    }

    /**
     * @return string
     */
    public static function getVersion(): string
    {
        return self::$version;
    }

    /**
     * @return string
     */
    public static function getProfileImagesPath(): string
    {
        return self::$_profileImagesPath;
    }

    public static function isThemeDark()
    {
        return self::getUserData('themeDark', false);
    }

    public static function getSidebarNavClasses(): string
    {
        $data = (array)self::getUserData('customize', []);
        $class = '';
        $classes = [
            'flat_style' => 'nav-flat',
            'inline_style' => 'nav-legacy',
            'compact_style' => 'nav-compact',
            'child_indent_style' => 'nav-child-indent',
            'child_hide' => 'nav-collapse-hide-child'
        ];

        foreach ($classes as $key => $item) {
            if (Arr::has($data, $key)) {
                if ($data[$key] == true) {
                    $class .= ' ' . $item;
                }
            }
        }

        return $class;
    }

    /**
     * Извлечение пользователя вместе с UserData
     * @param User|int $user
     * @return stdClass
     */
    public static function getUserForData($user)
    {
        if (gettype($user) == 'int') {
            $data = (array)json_decode(self::getDataByUserId($user)->data);
            $user = User::find($user)->toArray();
        } else {
            $data = (array)json_decode(self::getDataByUserId($user->id)->data);
            $user = $user->toArray();
        }

        return ((object)array_merge($user, $data));
    }

    public function getResource($path): string
    {
        return asset((config('admin.resource_path')
                ? DIRECTORY_SEPARATOR . ltrim(config('admin.resource_path'), DIRECTORY_SEPARATOR)
                : config('admin.resource_path')) . DIRECTORY_SEPARATOR
            . ltrim(
                ($path ? DIRECTORY_SEPARATOR . ltrim($path, DIRECTORY_SEPARATOR) : $path),
                DIRECTORY_SEPARATOR
            ));
    }

    public function getProfilePhoto(): string
    {
        $filename = $this->getUserData('photo', null);

        if ($filename != null) {
            $path = public_path($this->getProfileImagesPath() . '/' . $filename);
            if (file_exists($path) && !is_dir($path)) {
                return asset($this->getProfileImagesPath() . '/' . $filename);
            }
        }

        return $this->getResource('/images/user2-160x160.jpg');
    }

    public static function setResourcePath(string $path)
    {
        self::$_resourcePaths[] = $path;
    }

    public static function getResourcePaths(): array
    {
        return self::$_resourcePaths;
    }

    public static function setLanguage(string $language): void
    {
        self::$_languages[] = $language;
    }

    public static function getLanguages(): array
    {
        return self::$_languages;
    }

    public function setLastVisit()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $user->last_visit = time();
            $user->save();
        }
    }
}
