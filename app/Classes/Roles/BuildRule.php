<?php

namespace maaxim\admincore\Classes\Roles;

class BuildRule
{
    /**
     * @var Rule $_role
     */
    protected Rule $_role;

    public function __construct()
    {
        $this->make();
    }

    protected function make()
    {
        $this->_role = new Rule();
    }

    /**
     * @param Rule $role
     * @return BuildRule
     */
    public function setRole(Rule $role): BuildRule
    {
        $this->_role = $role;
        return $this;
    }

    /**
     * @return Rule
     */
    public function getRole(): Rule
    {
        return $this->_role;
    }

    /**
     * @param bool $val
     * @return $this
     */
    public function setAdminPanel(bool $val): BuildRule
    {
        $this->_role->setAdminPanel($val);
        return $this;
    }

    /**
     * @param string $group - page key
     * @param string $abbreviation - page name
     * @param PageRule $rules
     * @param bool $accept
     * @return BuildRule
     */
    public function setPageGroup(string $group, string $abbreviation, PageRule $rules, bool $accept = false): BuildRule
    {
        $this->_role->setPageGroup($group, $abbreviation, $rules, $accept);
        return $this;
    }

    public function getRules(): array
    {
        return $this->_role->getParams();
    }

    public function includeMappingFile($file)
    {
        if (file_exists($file) && !is_dir($file)) {
            require_once $file;
        }
    }
}
