<?php


namespace maaxim\admincore\Classes\Roles;

class Rule
{
    protected array $_defaultValues = [
        'adminPanel' => [
            'value' => false,
            'abbreviation' => 'Доступ к AdminPanel'
        ]
    ];
    protected array $_params = [];

    /**
     * @param bool $val
     */
    public function setAdminPanel(bool $val)
    {
        $this->_params['adminPanel']['value'] = $val;
        $this->_params['adminPanel']['abbreviation'] = $this->_defaultValues['adminPanel']['abbreviation'];
    }

    /**
     * @param string $group - page key
     * @param string $abbreviation - page name
     * @param PageRule $rules
     * @param bool $accept
     */
    public function setPageGroup(string $group, string $abbreviation, PageRule $rules, bool $accept = false)
    {
        $this->_params[$group]['abbreviation'] = $abbreviation;
        $this->_params[$group]['accept'] = $accept;
        $this->_params[$group]['params'] = $rules->getParams();

        $this->_defaultValues[$group]['abbreviation'] = $abbreviation;
        $this->_defaultValues[$group]['accept'] = $accept;
        $this->_defaultValues[$group]['params'] = $rules->getParams();
    }

    /**
     * @param array $params
     * @return Rule
     */
    public function setParams(array $params): Rule
    {
        $this->_params = $params;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return array_replace_recursive($this->_defaultValues, $this->_params);
    }
}
