<?php

namespace maaxim\admincore\Classes\Roles;

class PageRule
{
    protected array $_defaultValues = [
        'add' => [
            'value' => false,
            'abbreviation' => 'Добавление'
        ],
        'edit' => [
            'value' => false,
            'abbreviation' => 'Редактирование'
        ],
        'delete' => [
            'value' => false,
            'abbreviation' => 'Удаление'
        ],
    ];

    protected bool $_customParams = false;
    protected array $_params = [];

    /**
     * @param bool $val
     * @return PageRule
     */
    public function setAdd(bool $val): PageRule
    {
        $this->_params['add']['value'] = $val;
        $this->_params['add']['abbreviation'] = $this->_defaultValues['add']['abbreviation'];
        return $this;
    }

    /**
     * @param bool $val
     * @return PageRule
     */
    public function setEdit(bool $val): PageRule
    {
        $this->_params['edit']['value'] = $val;
        $this->_params['edit']['abbreviation'] = $this->_defaultValues['edit']['abbreviation'];
        return $this;
    }

    /**
     * @param bool $val
     * @return PageRule
     */
    public function setDelete(bool $val): PageRule
    {
        $this->_params['delete']['value'] = $val;
        $this->_params['delete']['abbreviation'] = $this->_defaultValues['delete']['abbreviation'];
        return $this;
    }

    /**
     * @param string $rule
     * @param bool $val
     * @param string $abbreviation
     * @return PageRule
     */
    public function setCustomRule(string $rule, bool $val, string $abbreviation): PageRule
    {
        $this->_params[$rule]['value'] = $val;
        $this->_params[$rule]['abbreviation'] = $abbreviation;
        return $this;
    }

    public function setCustomParams(array $params): PageRule
    {
        $this->_customParams = true;
        $this->_params = $params;
        return $this;
    }

    public function getParams(): array
    {
        if (!$this->_customParams) return array_replace_recursive($this->_defaultValues, $this->_params);
        else return $this->_params;
    }
}
