<?php

namespace maaxim\admincore\Classes\Links;

use maaxim\admincore\Classes\Links\Html\Link;
use maaxim\admincore\Classes\Links\Html\Group;

class HtmlBuilder
{
    use Link, Group;

    protected array $_params;

    public function __construct(array $params)
    {
        $this->_params = $params;
    }

    public function runBuild(): string
    {
        if ($this->_params['group'] !== false) {
            if (count($this->_params['group']) > 0) {
                return $this->createGroup($this->createGroupObject());
            }
        }

        return $this->getBaseLink();
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->_params;
    }
}
