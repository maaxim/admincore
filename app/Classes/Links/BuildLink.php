<?php

namespace maaxim\admincore\Classes\Links;

use Closure;
use Illuminate\Support\Arr;
use maaxim\admincore\Facades\Role;
use maaxim\admincore\Classes\Roles\PageRule;
use maaxim\admincore\Exceptions\LinkException;

class BuildLink
{
    protected array $_links = [];
    protected int $_lastAddLink;

    public function add(array $params = []): BuildLink
    {
        $key = count($this->_links);
        $this->_links[$key] = new Link($params);
        $this->_lastAddLink = $key;

        return $this;
    }

    protected function getLink(): Link
    {
        return $this->_links[$this->_lastAddLink];
    }

    public function name(string $name): BuildLink
    {
        $this->getLink()->setName($name);
        return $this;
    }

    public function route(string $route): BuildLink
    {
        $this->getLink()->setRoute($route);
        return $this;
    }

    public function icon(string $icon): BuildLink
    {
        $this->getLink()->setIcon($icon);
        return $this;
    }

    /**
     * @param Closure|string|int $value
     * @param string $color
     *
     * @return BuildLink
     */
    public function badge($value, string $color): BuildLink
    {
        $this->getLink()->setBadge($value, $color);
        return $this;
    }

    /**
     * @param string|array $routes
     * @return $this
     * @throws LinkException
     */
    public function active($routes = ''): BuildLink
    {
        $this->getLink()->setActive($routes);
        return $this;
    }

    public function group(Closure $callback): BuildLink
    {
        $this->getLink()->createGroup($callback);
        return $this;
    }

    /**
     * @param $group
     * @param $abbreviation
     * @param PageRule|null $rules
     * @param bool $accept
     * @return $this
     *
     * @deprecated В будущем будет удалена, в связи с выносом объявления правил в отдельном файле.
     */
    public function setPageRules($group, $abbreviation, PageRule $rules = null, bool $accept = false): BuildLink
    {
        if ($rules == null) $rules = new PageRule();
        Role::setPageGroup($group, $abbreviation, $rules, $accept);
        return $this;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->_links;
    }

    public function render(): string
    {
        $html = '';

        foreach ($this->getLinks() as $link) {
            $html .= $link->getHtml();
        }

        return $html;
    }

    public function includeMappingFile($file)
    {
        if (file_exists($file) && !is_dir($file)) {
            require_once $file;
        }
    }
}
