<?php


namespace maaxim\admincore\Classes\Links;

use Closure;
use maaxim\admincore\Exceptions\LinkException;

class Link
{
    protected array $_params = [];

    protected array $_paramsDefaultValues = [
        'name' => '',
        'route' => '',
        'icon' => '',
        'badge' => false,
        'active' => '',
        'group' => false
    ];

    public function __construct(array $params = [])
    {
        $this->_params = $params;
    }

    public function setName(string $name)
    {
        $this->_params['name'] = $name;
    }

    public function setRoute(string $route)
    {
        $this->_params['route'] = $route;
    }

    public function setIcon(string $icon)
    {
        $this->_params['icon'] = $icon;
    }

    public function setBadge($value, string $color)
    {
        if ($value instanceof Closure) {
            $value = $value();
        }

        $this->_params['badge'] = [
            'value' => $value,
            'color' => $color
        ];
    }

    public function setActive($active = '')
    {
        if (is_string($active)) {
            $this->_params['active'] = $active;
        } elseif (is_array($active)) {
            $this->_params['active'] = [];

            foreach ($active as $item) {
                $this->_params['active'][] = $item;
            }
        } else {
            throw new LinkException("active param: должен быть string|array", 500);
        }
    }

    public function createGroup(Closure $callback)
    {
        $groupLinks = new BuildLink();
        $callback($groupLinks);

        $this->_params['group'] = $groupLinks->getLinks();
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return array_replace_recursive($this->_paramsDefaultValues, $this->_params);
    }

    public function getHtml(): string
    {
        return (new HtmlBuilder($this->getParams()))->runBuild();
    }
}
