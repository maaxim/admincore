<?php

namespace maaxim\admincore\Classes\Links\Html;

use maaxim\admincore\Classes\Links\HtmlBuilder;

trait Group
{
    use Link, Func;

    public function createGroupObject(): array
    {
        return !$this->parentLinkHasBadge() ? Elements::$groupLink : Elements::$groupBadgeLink;
    }

    public function createGroup($groupObject): string
    {
        $groupObject[0] = '<li class="nav-item has-treeview' . ($this->parentLinkActive() ? ' menu-open' : '') . '">';
        $groupObject[1] = '<a href="%s" class="nav-link' . ($this->parentLinkActive() ? ' active' : '') . '">';

        $groupObject[1] = sprintf($groupObject[1],
            $this->_params['route'] !== '' ? route($this->_params['route']) : '#'
        );

        $this->parentLinkHasBadge();

        $groupObject[2] = sprintf($groupObject[2],
            $this->_params['icon']
        );

        $groupObject[4] = sprintf($groupObject[4],
            $this->_params['name']
        );

        $parentsKey = !$this->parentLinkHasBadge() ? 9 : 10;
        $groupObject[$parentsKey] = sprintf($groupObject[$parentsKey],
            $this->getParentLinks()
        );

        return $this->getHtmlEl($groupObject);
    }

    protected function getParentLinks(): string
    {
        $html = '';

        foreach ($this->_params['group'] as $link) {
            $html .= (new HtmlBuilder($link->getParams()))->runBuild();
        }

        return $html;
    }

    protected function parentLinkActive(): bool
    {
        $active = false;

        foreach ($this->_params['group'] as $link) {
            $linkActive = (new HtmlBuilder($link->getParams()))->activeLink();

            if ($linkActive) {
                $active = true;
                break;
            }
        }

        return $active;
    }

    protected function parentLinkHasBadge(): bool
    {
        $has = false;

        foreach ($this->_params['group'] as $link) {
            if ($link->getParams()['badge'] != false) {
                $has = true;
                break;
            }
        }

        return $has;
    }
}
