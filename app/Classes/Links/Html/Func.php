<?php

namespace maaxim\admincore\Classes\Links\Html;

trait Func
{
    protected function getHtmlEl($html): string
    {
        return implode('', $html);
    }
}
