<?php


namespace maaxim\admincore\Classes\Links\Html;


class Elements
{
    public static array $groupLink = [
        '<li class="nav-item has-treeview">',      // 1
        '<a href="%s" class="nav-link">',          // 2
        '<i class="nav-icon %s"></i>',             // 3
        '<p>',                                     // 4
        '%s',                                      // 5
        '<i class="right fas fa-angle-left"></i>', // 6
        '</p>',                                    // 7
        '</a>',                                    // 8
        '<ul class="nav nav-treeview">',           // 9
        '%s',                                      // 10
        '</ul>',                                   // 11
        '</li>'                                    // 12
    ];

    public static array $baseLink = [
        '<li class="nav-item">',              // 1
        '<a href="%s" class="nav-link">',     // 2
        '<i class="nav-icon %s"></i>',        // 3
        '<p>',                                // 4
        '%s',                                 // 5
        '</p>',                               // 6
        '</a>',                               // 7
        '</li>'                               // 8
    ];

    public static array $groupBadgeLink = [
        '<li class="nav-item has-treeview">',                // 1
        '<a href="%s" class="nav-link">',                    // 2
        '<i class="nav-icon %s"></i>',                       // 3
        '<p>',                                               // 4
        '%s',                                                // 5
        '<i class="right fas fa-angle-left"></i>',           // 6
        '<span class="right badge badge-primary">!</span>',  // 7
        '</p>',                                              // 8
        '</a>',                                              // 9
        '<ul class="nav nav-treeview">',                     // 10
        '%s',                                                // 11
        '</ul>',                                             // 12
        '</li>'                                              // 13
    ];

    public static array $badgeLink = [
        '<li class="nav-item">',                  // 1
        '<a href="%s" class="nav-link">',         // 2
        '<i class="nav-icon %s"></i>',            // 3
        '<p>',                                    // 4
        '%s',                                     // 5
        '<span class="right badge %s">%s</span>', // 6
        '</p>',                                   // 7
        '</a>',                                   // 8
        '</li>'                                   // 9
    ];
}
