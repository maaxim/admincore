<?php

namespace maaxim\admincore\Classes\Links\Html;

trait Link
{
    use Func;

    public function getBaseLink(): string
    {
        if ($this->_params['badge'] == false) {
            $html = Elements::$baseLink;
        } else {
            $html = Elements::$badgeLink;

            $html[5] = sprintf($html[5],
                'badge-' . $this->_params['badge']['color'],
                $this->_params['badge']['value']
            );
        }

        $html[1] = '<a href="%s" class="nav-link' . ($this->activeLink() ? ' active' : '') . '">';

        $html[1] = sprintf($html[1],
            $this->_params['route'] !== '' ? route($this->_params['route']) : '#'
        );
        $html[2] = sprintf($html[2],
            $this->_params['icon']
        );
        $html[4] = sprintf($html[4],
            $this->_params['name']
        );

        return $this->getHtmlEl($html);
    }

    public function activeLink(): bool
    {
        if ($this->_params['active'] != '') {
            if (is_string($this->_params['active'])) {
                if (app('router')->currentRouteNamed($this->_params['active'])) return true;
            } else {
                foreach ($this->_params['active'] as $item) {
                    if (app('router')->currentRouteNamed($item)) return true;
                }
            }
        }

        return app('router')->currentRouteNamed($this->_params['route']);
    }
}
