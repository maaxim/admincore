<?php

namespace maaxim\admincore\Classes;

use Illuminate\Support\Arr;

class Breadcrumbs
{
    protected array $_links = [];

    public function __construct()
    {
        $this->make();
    }

    protected function make()
    {
        $uri = app('router')->current()->uri;
        $group_full = explode('/', $uri);
        Arr::forget($group_full, 0);
        $group = [];

        foreach ($group_full as $item) {
            $group[] = $item;
        }

        $sub_groups = $this->replaceGroup($this->buildSubGroup($group));

        $this->_links[] = [
            'title' => 'Главная',
            'href' => '/admin',
            'active' => (count($sub_groups) >= 1)
        ];

        foreach ($sub_groups as $key => $item) {
            $href = explode('/', $item);

            $this->_links[] = [
                'title' => __($href[count($href) - 1]),
                'href' => $item,
                'active' => ((count($sub_groups) - 1) > $key)
            ];
        }
    }

    protected function replaceGroup(array $group): array
    {
        $group_arr = [];

        foreach ($group as $key => $item) {
            $gr = explode('/', $item);
            $string = '';

            foreach ($gr as $key2 => $item2) {
                $param = str_split($item2);
                if ($param[0] == "{" && $param[count($param) - 1] == "}") {
                    $param = str_split($gr[$key2 - 1]);
                    $param[count($param) - 1] = '';
                    $word = '';

                    foreach ($param as $let) {
                        $word .= $let;
                    }

                    $string .= '/' . $word;
                    continue;
                }

                if ($item2 != '') {
                    $string .= '/' . $item2;
                }
            }

            $group_arr[] = $string;
        }

        return $group_arr;
    }

    protected function buildSubGroup(array $group): array
    {
        $rGroup = [];

        foreach ($group as $item) {
            $string = "/admin";

            for ($i = 0; count($group) > $i; $i++) {
                $string .= '/' . $group[$i];
            }

            $rGroup[] = $string;
            $i -= 1;
            Arr::forget($group, $i);
        }

        return array_reverse($rGroup);
    }

    public function get(): array
    {
        return $this->_links;
    }
}