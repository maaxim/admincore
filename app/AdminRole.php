<?php

namespace maaxim\admincore;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminRole
 * @method static find($id)
 * @method static findOrFail($id)
 * @method static where($col, $dis = '', $val = '')
 *
 * @property int role_id
 * @property string role_name
 * @property string|array rules
 * @package maaxim\admincore
 */
class AdminRole extends Model
{
    protected $primaryKey = 'role_id';
    protected $table = 'roles';
}
