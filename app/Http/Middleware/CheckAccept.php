<?php

namespace maaxim\admincore\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use maaxim\admincore\AdminRole;
use maaxim\admincore\Facades\Role;
use Illuminate\Support\Facades\Auth;
use maaxim\admincore\Exceptions\RoleException;

class CheckAccept
{
    public function handle(Request $request, Closure $next)
    {
        $page = $request->input('page', null);
        $role = AdminRole::findOrFail(Auth::user()->role_id);

        $rules = json_decode(json_encode(
            Role::setRole(
                Role::getRole()
                    ->setParams((array) json_decode($role->rules))
            )->getRules()
        ));

        $response = $request->expectsJson()
            ? response()->json([
                'title' => 'Доступ запрещён',
                'text' => "Для вашей роли \"{$role->role_name}\" установлено ограничение на эту страницу"
            ], 401
        )
            : new Response(view('admin::errors.no_accept', [
            'title' => 'Доступ запрещён',
            'role_name' => $role->role_name
        ]));

        if ($page == null) {
            $page = explode('/', url()->current());
            $page = $page[(count($page) - 1)];

            if ($this->hasPage($rules, $page)) {
                if (!$rules->$page->accept) {
                    return $response;
                }
            }
        } else {
            $page = explode('.', $page);

            if ($this->hasPage($rules, $page[0])) {
                if (count($page) == 1) {
                    $page = $page[0];
                    if (!$rules->$page->accept) {
                        return $response;
                    }
                } else {
                    $page_root = $page[0];
                    $page_sub = $rules->$page_root->params;
                    $page = (object) Arr::except((array) $page, [0]);

                    foreach ($page as $item) {
                        $page_sub = $page_sub->$item;
                    }

                    if (!$page_sub->value) {
                        return $response;
                    }
                }
            }
        }
        return $next($request);
    }

    protected function hasPage($rules, $page)
    {
        if (Arr::has((array) $rules, $page)) {
            return true;
        } else {
            throw new RoleException("В таблице правил страницы [{$page}] нет", 500);
        }
    }
}
