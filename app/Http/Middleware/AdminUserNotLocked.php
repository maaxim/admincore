<?php

namespace maaxim\admincore\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminUserNotLocked
{
    public function handle(Request $request, Closure $next)
    {
        if (!$request->route()->named(['AdminLockscreen', 'AdminLockscreenUnlock']) && Auth::check())
        {
            if ($request->cookie('admin_user_lock') == '1') {
                return redirect()->route('AdminLockscreen');
            }
        }

        return $next($request);
    }
}
