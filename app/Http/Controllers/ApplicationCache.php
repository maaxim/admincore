<?php

namespace maaxim\admincore\Http\Controllers;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

class ApplicationCache extends AdminController
{
    protected $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function cacheClear()
    {
        Artisan::call('cache:clear');
        Artisan::call('event:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        Artisan::call('route:clear');
        Artisan::call('optimize:clear');

        return response()->json(true);
    }

    public function optimize()
    {
        $routeCached = true;

        foreach (Route::getRoutes()->getRoutes() as $route) {
            if ($route->getAction()['uses'] instanceof Closure) {
                $routeCached = false;
                break;
            }
        }

        if ($routeCached == true) Artisan::call('route:cache');

        Artisan::call('event:cache');
        Artisan::call('config:cache');
        Artisan::call('view:cache');

        return response()->json(true);
    }
}
