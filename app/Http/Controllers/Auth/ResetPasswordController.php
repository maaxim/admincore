<?php

namespace maaxim\admincore\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use maaxim\admincore\AdminResetPassword;

class ResetPasswordController extends Controller
{
    protected $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function page($token)
    {
        $this->getToken($token);

        return view('admin::auth.reset_password', [
            'token' => $token,
            'email' => $this->_request->get('email')
        ]);
    }

    public function reset($token)
    {
        $token = $this->getToken($token);

        $rules = [
            'email' => 'required|max:255|email',
            'password' => 'required|min:6|string|confirmed'
        ];
        $this->validate($this->_request, $rules);

        $user = User::find($token->user_id);
        $user->email = $this->_request->input('email');
        $user->password = Hash::make($this->_request->input('password'));
        $user->save();

        $token->delete();

        return redirect()->route('AdminLogin');
    }

    public function getToken($token)
    {
        $token = AdminResetPassword::where('token', $token)->get()->toArray();

        if (empty($token)) {
            return abort(404);
        }

        return AdminResetPassword::find($token[0]['id']);
    }
}
