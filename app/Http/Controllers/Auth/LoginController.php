<?php


namespace maaxim\admincore\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo;
    protected $_request;

    public function __construct(Request $request)
    {
        $this->redirectTo = route('AdminHome');
        $this->_request = $request;
    }

    public function showLoginForm()
    {
        return view('admin::auth.login');
    }

    public function username()
    {
        return 'login';
    }

    public function firstLogin($key, $id)
    {
        $user = User::findOrFail($id);

        if (Hash::check($key, $user->password)) {
            return view('admin::auth.first_login', [
                'user' => $user,
                'email' => $user->email,
                'key' => $key
            ]);
        } else {
            return abort(404);
        }
    }

    public function firstLoginSave($key, $id)
    {
        $user = User::findOrFail($id);

        if (Hash::check($key, $user->password)) {
            $rules = [
                'name' => 'required|max:255|string',
                'login' => 'required|max:255|string',
                'password' => 'required|min:6|string|confirmed'
            ];

            if ($user->login != $this->_request->input('login')) {
                $rules['login'] .= '|unique:users';
            }
            $this->validate($this->_request, $rules);

            $user->name = $this->_request->input('name');
            $user->login = $this->_request->input('login');
            $user->password = Hash::make($this->_request->input('password'));
            $user->save();

            return redirect()->route('AdminLogin');
        } else {
            return abort(404);
        }
    }
}
