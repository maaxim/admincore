<?php

namespace maaxim\admincore\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use maaxim\admincore\Facades\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Profile extends Controller
{
    protected Request $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function page()
    {
        return view('admin::auth.profile', [
            'title' => 'Профиль'
        ]);
    }

    public function getUserData(): JsonResponse
    {
        $user = Auth::user()->toArray();
        $user_data = Admin::getUserData();
        $user_datum = json_decode($user_data->data);

        $user = Arr::add(
            $user,
            'themeDark',
            $user_datum->themeDark ?? false
        );
        $user = Arr::add(
            $user,
            'lockScreenTimeout',
            $user_datum->lockScreenTimeout ?? '20'
        );

        return response()->json($user);
    }

    public function save(): JsonResponse
    {
        if ($this->_request->isMethod('post')) {
            $rules = [
                'name' => 'required|max:255',
                'login' => 'required|max:255',
                'email' => 'required|max:255|email'
            ];

            if ($this->_request->login != Auth::user()->login) {
                $rules['login'] .= '|unique:users';
            }

            if ($this->_request->email != Auth::user()->email) {
                $rules['email'] .= '|unique:users';
            }

            $this->validate($this->_request, $rules);

            if ($this->_request->hasFile('photo')) {
                $file = $this->_request->file('photo');
                $fileName = $file->hashName();
                $file->move(public_path(Admin::getProfileImagesPath()), $fileName);
            }

            $user = Auth::user();
            $user->name = $this->_request->name;
            $user->login = $this->_request->login;
            $user->email = $this->_request->email;

            if ($this->_request->hasFile('photo')) {
                Admin::setUserData([
                    'photo' => $fileName
                ]);
            }

            $user->save();

            return response()->json(true);
        }

        return response()->json(false);
    }

    public function personalization(): JsonResponse
    {
        Admin::setUserData([
            'themeDark' => $this->_request->input('themeDark', false),
            'lockScreenTimeout' => $this->_request->input('lockScreenTimeout', 20)
        ]);

        return response()->json(true);
    }

    public function setDarkTheme(): JsonResponse
    {
        Admin::setUserData([
            'themeDark' => $this->_request->input('theme_dark', false),
        ]);

        return response()->json(true);
    }

    public function customize()
    {
        Admin::setUserData([
            'customize' => [
                'header_fixed' => $this->_request->input(
                    'header_fixed',
                    Admin::getUserData('customize.header_fixed', true)
                ),
                'header_no_border' => $this->_request->input(
                    'header_no_border',
                    Admin::getUserData('customize.header_no_border', false)
                ),
                'sidebar_collapse' => $this->_request->input(
                    'sidebar_collapse',
                    Admin::getUserData('customize.sidebar_collapse', false)
                ),
                'layout_fixed' => $this->_request->input(
                    'layout_fixed',
                    Admin::getUserData('customize.layout_fixed', true)
                ),
                'flat_style' => $this->_request->input(
                    'flat_style',
                    Admin::getUserData('customize.flat_style', false)
                ),
                'inline_style' => $this->_request->input(
                    'inline_style',
                    Admin::getUserData('customize.inline_style', false)
                ),
                'compact_style' => $this->_request->input(
                    'compact_style',
                    Admin::getUserData('customize.compact_style', false)
                ),
                'child_indent_style' => $this->_request->input(
                    'child_indent_style',
                    Admin::getUserData('customize.child_indent_style', false)
                ),
                'off_auto_open_style' => $this->_request->input(
                    'off_auto_open_style',
                    Admin::getUserData('customize.off_auto_open_style', false)
                ),
                'child_hide' => $this->_request->input(
                    'child_hide',
                    Admin::getUserData('customize.child_hide', false)
                ),
                'footer_fixed' => $this->_request->input(
                    'footer_fixed',
                    Admin::getUserData('customize.footer_fixed', true)
                ),
                'bg_top_nav' => $this->_request->input(
                    'bg_top_nav',
                    Admin::getUserData('customize.bg_top_nav', 'navbar-dark')
                ),
                'bg_main_link' => $this->_request->input(
                    'bg_main_link',
                    Admin::getUserData('customize.bg_main_link', 'sidebar-dark-primary')
                ),
                'bg_logo' => $this->_request->input(
                    'bg_logo',
                    Admin::getUserData('customize.bg_logo', 'navbar-dark')
                ),
            ]
        ]);
    }

    public function resetPassword(): JsonResponse
    {
        if ($this->_request->isMethod('post')) {
            $this->validate($this->_request, [
                'password' => 'required|string|min:6|confirmed'
            ]);

            $user = User::find(Auth::id());
            $user->password = Hash::make($this->_request->password);
            $user->save();

            return response()->json(true);
        }

        return response()->json(false);
    }

    public function getLockScreenTimeout(): JsonResponse
    {
        return response()->json([
            'time' => Admin::getUserData('lockScreenTimeout', '20')
        ]);
    }

    public function checkPassword($json = true): JsonResponse
    {
        return response()->json(
            Hash::check($this->_request->password, Auth::user()->getAuthPassword())
        );
    }

    public function isOnline(\Illuminate\Foundation\Auth\User $user = null)
    {
        if ($user == null && $this->_request->expectsJson()) {
            $user = User::findOrFail($this->_request->input('user_id'));
        } elseif ($user == null && !$this->_request->expectsJson()) {
            $user = Auth::user();
        }

        if (time() >= ($user->last_visit + (1 * 60))) {
            return $this->_request->expectsJson()
                ? response()->json(false)
                : false;
        } else {
            return $this->_request->expectsJson()
                ? response()->json(true)
                : true;
        }
    }

    public function getLastOnline(\Illuminate\Foundation\Auth\User $user = null)
    {
        if ($user == null && $this->_request->expectsJson()) {
            $user = User::findOrFail($this->_request->input('user_id'));
        } elseif ($user == null && !$this->_request->expectsJson()) {
            $user = Auth::user();
        }
        $date = date('H:i d.m.Y', $user->last_visit);

        return $this->_request->expectsJson()
            ? response()->json($date)
            : $date;
    }
}
