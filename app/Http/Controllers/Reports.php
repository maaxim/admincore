<?php

namespace maaxim\admincore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use maaxim\admincore\ErrorReport;
use Illuminate\Support\Facades\DB;

class Reports extends AdminController
{
    protected Request $_request;

    public function __construct(Request $request)
    {
        $this->setRulePage([
            'AdminReportFix' => 'reports.fixed',
            'AdminReportDelete' => 'reports.delete'
        ], 'reports');
        $this->checkRules($request, ['report']);

        $this->_request = $request;
    }

    public function page()
    {
        return view('admin::pages.reports', [
            'title' => 'Ошибки при работе сайта',
            'reports' => ErrorReport::orderBy('id', 'desc')->get(),
            'count' => 1
        ]);
    }

    public function report($id)
    {
        $report = ErrorReport::findOrFail($id);
        $report->read = true;
        $report->save();

        return view('admin::pages.report', [
            'title' => 'Ошибка при работе сайта',
            'small' => '# ' . $report->id,
            'error' => $report
        ]);
    }

    public function fixed(): JsonResponse
    {
        $report = ErrorReport::findOrFail($this->_request->input('id'));
        $report->fixed = true;
        $report->save();

        return response()->json(true);
    }

    public function delete(): JsonResponse
    {
        ErrorReport::findOrFail($this->_request->input('id'))->delete();
        return response()->json(route('AdminReports'));
    }

    public function clearTable(): JsonResponse
    {
        DB::statement("TRUNCATE TABLE error_reports;");
        return response()->json(true);
    }
}
