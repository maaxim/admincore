<?php

namespace maaxim\admincore\Http\Controllers;

use App\Http\Controllers\Controller;
use maaxim\admincore\Http\Traits\SetPageRules;

class AdminController extends Controller
{
    use SetPageRules;
}
