<?php

namespace maaxim\admincore\Http\Controllers;

use App\Models\User;
use DateTimeImmutable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use maaxim\admincore\AdminData;
use maaxim\admincore\AdminRole;
use maaxim\admincore\Facades\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use maaxim\admincore\AdminResetPassword;
use maaxim\admincore\Mail\AdminRegisterUserMail;
use maaxim\admincore\Mail\AdminResetPasswordMail;
use maaxim\admincore\Http\Controllers\Auth\Profile;

class Users extends AdminController
{
    protected Request $_request;
    protected $userDelimiterPassword;

    public function __construct(Request $request)
    {
        $this->setRulePage([
            'AdminUsersAdd' => 'users.add',
            'AdminUsersEdit' => 'users.edit',
            'AdminUsersDelete' => 'users.delete',
            'AdminUsersResetPassword' => 'users.reset'
        ], 'users');
        $this->checkRules($request, ['getPage', 'getUsers', 'getUserRegisterLink']);

        $this->_request = $request;
    }

    public function page()
    {
        return view('admin::pages.users', [
            'title' => 'Пользователи',
            'users' => json_encode($this->getUsers()),
            'roles' => json_encode(AdminRole::all())
        ]);
    }

    public function getUsers()
    {
        if (!$this->_request->has('id')) {
            $users = User::orderBy('id', 'desc')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.role_id')->get();
            $users_data = [];

            foreach ($users as $user) {
                $item = collect(Admin::getUserForData($user))->toArray();
                $item = Arr::add($item, 'isOnline', (new Profile($this->_request))->isOnline($user));
                $item = Arr::add($item, 'lastOnline', (new Profile($this->_request))->getLastOnline($user));
                $users_data[] = $item;
            }

            foreach ($users_data as $key => $users_datum) {
                $users_data[$key] = $this->formatUserData($users_datum);
            }
        } else {
            $user = User::findOrFail($this->_request->input('id'));
            $users_data = collect(Admin::getUserForData($user))->toArray();
            $users_data = $this->formatUserData($users_data);
        }

//        dd($users_data);

        return $this->_request->expectsJson()
            ? response()->json($users_data)
            : $users_data;
    }

    protected function formatUserData(array $user)
    {
        $user = Arr::except($user, [
            'updated_at', 'email_verified_at', 'rules', 'themeDark', 'customize', 'lockScreenTimeout'
        ]);
        $user['created_at'] = date_format(
            new DateTimeImmutable($user['created_at']),
            'H:i d.m.Y'
        );

        $user_other_data = Arr::except($user, [
            'id', 'name', 'login', 'last_visit', 'created_at', 'role_id', 'role_name', 'isOnline', 'lastOnline'
        ]);

        foreach ($user_other_data as $key => $item) {
            $user_other_data[$key] = [
                'title' => __($key),
                'value' => $item
            ];
        }

        $user['user_data'] = $user_other_data;

        return $user;
    }

    public function deleteUser()
    {
        $id = $this->_request->input('id');
        $user = User::findOrFail($id);
        $user_data = AdminData::where('user_id', $id)->get()->toArray();

        if (!empty($user_data)) {
            $user_data = AdminData::find($user_data[0]['data_id']);
            $user_data->delete();
        }

        $photo = Admin::getDataByUserId($id, 'photo', null);
        $path = Admin::getProfileImagesPath() . '/' . $photo;
        if ($photo != null) {
            if (file_exists(public_path($path))) {
                unlink(public_path($path));
            }
        }

        $user->delete();
    }

    public function addUser()
    {
        $rules = [
            'name' => 'required|max:255',
            'login' => 'required|max:255|unique:users',
            'password' => 'confirmed',
        ];

        if ($this->_request->setType == 'set') {
            $rules['password'] .= '|required';
        } elseif ($this->_request->setType == 'login') {
            $rules['email'] = 'required';
        }
        $this->validate($this->_request, $rules);

        $new = new User();
        $new->name = $this->_request->input('name');
        $new->login = $this->_request->input('login');
        $new->role_id = $this->_request->input('role');

        if ($this->_request->setType == 'login') {
            $new->password = Hash::make($this->_request->input('registerKey'));
            $new->email = $this->_request->input('email');
            $new->save();

            Mail::send((new AdminRegisterUserMail(
                $this->_request->input('registerKey'),
                $new->id,
                $this->_request->input('email')
            )));
        } elseif ($this->_request->setType == 'set') {
            $new->password = Hash::make($this->_request->input('password'));
            $new->save();
        } else {
            return response()->json(false);
        }

        return response()->json(true);
    }

    public function editUser()
    {
        $user = User::findOrFail($this->_request->input('id'));
        $user->role_id = $this->_request->input('role_id');
        $user->save();

        return response()->json(true);
    }

    public function resetPassword()
    {
        $user = User::findOrFail($this->_request->id);
        $token = Str::random(60);

        if ($user->role_id == 1) {
            return response()->json([
                'status' => 'error',
                'alert' => [
                    'icon' => 'warning',
                    'title' => 'Нельзя сбросить пароль для обычного пользователя'
                ]
            ]);
        }

        if (($email = $this->_request->input('email')) == null) {
            return response()->json(['status' => 'email']);
        }

        $reset = new AdminResetPassword();
        $reset->user_id = $user->id;
        $reset->email = $email;
        $reset->token = $token;
        $reset->save();

        Mail::send(new AdminResetPasswordMail($token, $email));

        return response()->json(true);
    }

    public function getUserRegisterLink()
    {
        $number = random_int(100000, 999999);

        return response()->json([
            'key' => $number
        ]);
    }
}
