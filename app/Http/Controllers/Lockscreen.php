<?php

namespace maaxim\admincore\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;

class Lockscreen extends AdminController
{
    protected $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function page()
    {
        return view('admin::errors.lockscreen');
    }

    public function unlock()
    {
        if ($this->checkPassword()) {
            $cookie = Cookie::forget('admin_user_lock');

            return response()->json([
                'status' => true,
                'redirectLocation' => url()->previous('/admin') == route('AdminLockscreen')
                    ? url('/admin') : url()->previous()
            ])->withCookie($cookie);
        }

        return response()->json([
            'status' => false,
            'errors' => [
                [
                    'input' => 'password',
                    'error' => 'Пароль введён не верно'
                ]
            ]
        ]);
    }

    public function lock()
    {
        $json = $this->_request->input('json', false);
        $cookie = cookie('admin_user_lock', true, 9999);
        return $json ? response()
            ->json(route('AdminLockscreen'))
            ->cookie($cookie) : redirect()->route('AdminLockscreen')->cookie($cookie);
    }

    protected function checkPassword()
    {
        return Hash::check($this->_request->password, User::findOrFail(Auth::id())->password);
    }
}
