<?php

namespace maaxim\admincore\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use maaxim\admincore\AdminRole;
use maaxim\admincore\Facades\Role;

class Roles extends AdminController
{
    protected Request $_request;
    protected array $_without_interaction; // без взаимодействия, запрещено редактирование/удаление

    public function __construct(Request $request)
    {
        $this->setRulePage([
            'AdminRolesAdd' => 'roles.add',
            'AdminRolesEdit' => 'roles.edit',
            'AdminRoleDelete' => 'roles.delete',
        ], 'roles');
        $this->checkRules($request, ['getPage', 'getRules', 'getRoleName']);

        $this->_request = $request;
        $this->_without_interaction = config('admin.roles_without_interaction');
    }

    public function page()
    {
        return view('admin::pages.roles', [
            'title' => 'Роли пользователей',
            'roles' => AdminRole::all(),
            'count' => 1
        ]);
    }

    public function getRules()
    {
        return response()->json($this->replaceRulesForRender());
    }

    protected function replaceRulesForRender()
    {
        $rules = $this->_request->input('id', null) == null
            ? Role::getRules()
            : Role::setRole(
                Role::getRole()
                    ->setParams((array) json_decode(AdminRole::find($this->_request->input('id'))->rules))
            )->getRules();

        $new_arr_rules = [];
        $new_arr_rules['pages'] = Arr::except($rules, ['adminPanel', 'add', 'edit', 'delete']);
        $new_arr_rules['adminPanel'] = Arr::get($rules, 'adminPanel');
        $new_arr_rules['add'] = Arr::get($rules, 'add');
        $new_arr_rules['edit'] = Arr::get($rules, 'edit');
        $new_arr_rules['delete'] = Arr::get($rules, 'delete');

        return $new_arr_rules;
    }

    protected function replaceRulesForData($rules)
    {
        foreach ($rules['pages'] as $key => $item) {
            $rules[$key] = $item;
        }
        $rules = Arr::except($rules, 'pages');

        return $rules;
    }

    public function addPage()
    {
        return view('admin::pages.add.role', [
            'title' => 'Добавление',
            'small' => 'роли пользователя'
        ]);
    }

    public function add()
    {
        $this->validate($this->_request, [
            'name' => 'required|max:255|string|unique:' . (new AdminRole())->getTable() . ',role_name'
        ]);

        $new = new AdminRole();
        $new->role_name = $this->_request->input('name');
        $new->rules = json_encode($this->replaceRulesForData($this->_request->input('rules')));
        $new->save();

        return response()->json(true);
    }

    public function getRoleName()
    {
        return response()->json(AdminRole::findOrFail($this->_request->input('id'))->role_name);
    }

    public function editPage()
    {
        $editable = true;
        $id = $this->_request->get('id', null);
        AdminRole::findOrFail($id);

        foreach ($this->_without_interaction as $item) {
            if ($id == null) return abort(404);
            if ($item == $id) $editable = false;
        }

        return view('admin::pages.edit.role', [
            'title' => 'Редактирование',
            'small' => 'роли пользователя',
            'editable' => $editable
        ]);
    }

    public function edit()
    {
        $role = AdminRole::findOrFail($this->_request->input('id'));
        $rules = [
            'name' => 'required|max:255|string'
        ];

        if ($this->_request->input('name') != $role->role_name) {
            $rules['name'] .= '|unique:' . (new AdminRole())->getTable() . ',role_name';
        }

        $this->validate($this->_request, $rules);

        $role->role_name = $this->_request->input('name');
        $role->rules = json_encode($this->replaceRulesForData($this->_request->input('rules')));
        $role->save();

        $this->_request->session()->flash('success', 'test');

        return response()->json(true);
    }

    public function delete()
    {
        $id = $this->_request->input('id');

        foreach ($this->_without_interaction as $item) {
            if ($item == $id) {
                return response()->json("Эту роль нельзя удалить");
            }
        }

        $role = AdminRole::findOrFail($id);

        foreach (User::where('role_id', $id)->get() as $item) {
            $item->role_id = 1;
            $item->save();
        }

        $role->delete();

        return response()->json(true);
    }
}
