<?php

namespace maaxim\admincore\Http\Traits;

use Illuminate\Http\Request;

trait SetPageRules
{
    protected string $_rulePage = '';

    protected function getRulePage(): string
    {
        return $this->_rulePage;
    }

    protected function setRulePage(array $rules, string $default)
    {
        foreach ($rules as $key => $rule) {
            if (app('router')->currentRouteNamed($key)) {
                $this->_rulePage = $rule;
            }
        }

        if ($this->_rulePage == '') {
            $this->_rulePage = $default;
        }
    }

    protected function checkRules(Request $request, $except = null)
    {
        $request->merge(['page' => $this->getRulePage()]);
        $this->middleware('noAccept')->except($except);
    }
}
