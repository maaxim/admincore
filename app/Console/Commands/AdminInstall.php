<?php

namespace maaxim\admincore\Console\Commands;

use Illuminate\Support\Arr;
use maaxim\admincore\AdminRole;
use Illuminate\Console\Command;
use maaxim\admincore\Facades\Admin;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Finder\SplFileInfo;

class AdminInstall extends Command
{
    protected $signature = 'admin:install {type=all}';
    protected $description = 'Установка AdminPanel';

    protected array $_types = [
        'all' => [
            'publicConfigFile' => 'Публикация файла конфигурации',
            'publicLinksFile' => 'Публикация файла маршрутов(ссылки)',
            'publicRoutesFile' => 'Публикация файла маршрутов(роуты)',
            'publicSeedersFiles' => 'Публикация сидеров(seeders)',
            'publicResources' => 'Публикация ресурсов AdminPanel',
            'publicLangFiles' => 'Публикация файлов русского языка',
        ],
        'config' => [
            'publicConfigFile' => 'Публикация файла конфигурации'
        ],
        'links' => [
            'publicLinksFile' => 'Публикация файла маршрутов(ссылки)'
        ],
        'routes' => [
            'publicRoutesFile' => 'Публикация файла маршрутов(роуты)'
        ],
        'seeders' => [
            'publicSeedersFiles' => 'Публикация сидеров(seeders)'
        ],
        'resources' => [
            'publicResources' => 'Публикация ресурсов AdminPanel'
        ],
        'language' => [
            'publicLangFiles' => 'Публикация файлов русского языка'
        ],
    ];
    protected array $_seedersClassesName = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): int
    {
        if (Arr::has($this->_types, $this->argument('type'))) {
            foreach ($this->_types[$this->argument('type')] as $callback => $description) {
                $this->info($description);
                $this->$callback();
            }

            $this->runCommands();
            $this->info('Готово');
        } else {
            $this->info(
                "\nТип \"{$this->argument('type')}\" не найден. Доступны следующие типы:\n"
                . " • all\n"
                . " • config\n"
                . " • links\n"
                . " • routes\n"
                . " • seeders\n"
                . " • resources\n"
                . " • language\n"
            );
        }

        return 0;
    }

    protected function publicConfigFile()
    {
        copy(
            __DIR__ . '/../../../config/admin.stub',
            config_path('admin.php')
        );
    }

    protected function publicLinksFile()
    {
        if (!is_dir($dir = app_path('admin'))) {
            mkdir($dir, 0755, true);
        }

        copy(
            __DIR__ . '/../../admin/links.stub',
            app_path('admin/links.php')
        );

        copy(
            __DIR__ . '/../../admin/rules.stub',
            app_path('admin/rules.php')
        );
    }

    protected function publicRoutesFile()
    {
        copy(
            __DIR__ . '/../../../routes/admin.stub',
            base_path('routes/admin.php')
        );
    }

    protected function publicSeedersFiles()
    {
        $files = (new Filesystem)->allFiles(__DIR__ . '/../../../database/seeds', true);

        if (!is_dir($dir = database_path('seeders'))) {
            mkdir($dir, 0755, true);
        }

        collect($files)->filter(function (SplFileInfo $file) {
            $ex = explode('.', $file->getBasename());
            if ($ex[count($ex) - 1] == 'stub') {
                return true;
            }

            return false;
        })->each(function (SplFileInfo $file) use ($dir) {
            $this->_seedersClassesName[] = $file->getBasename('.stub');

            copy(
                $file->getRealPath(),
                $dir . '/' . $file->getBasename('.stub') . '.php'
            );
        });
    }

    protected function publicResources()
    {
        $config_path = config('admin.resource_path') == ''
            ? 'admin_resources'
            : config('admin.resource_path');

        if (file_exists(($dir = public_path($config_path))) && is_dir($dir)) {
            File::deleteDirectory($dir);
        }

        copyDir(
            __DIR__ . '/../../../resources/assets/admin',
            $dir
        );

        foreach (Admin::getResourcePaths() as $item) {
            if (file_exists($item) && is_dir($item)) {
                $path = explode('/', $item);
                $path = $path[count($path) - 1];
                $path = public_path($config_path . '/' . $path);

                copyDir(
                    $item,
                    $path
                );
            }
        }
    }

    protected function runCommands(): bool
    {
        if (Schema::hasTable((new AdminRole)->getTable())) {
            if (AdminRole::count() > 1) {
                return false;
            }
        }

        if (count($this->_seedersClassesName) == 0) {
            $this->publicSeedersFiles();
        }

        Artisan::call('package:discover --ansi');
        Artisan::call('migrate');

        foreach ($this->_seedersClassesName as $item) {
            Artisan::call("db:seed --class={$item}");
        }

        return true;
    }

    public function publicLangFiles()
    {
        $file = resource_path('lang/ru.json');
        if (!file_exists($file)) {
            copyDir(
                __DIR__ . '/../../../resources/lang',
                resource_path('lang')
            );
        }

        $baseLocalFile = json_decode(file_get_contents($file), true);
        foreach (Admin::getLanguages() as $language) {
            if (file_exists($language) && !is_dir($language)) {
                $content = json_decode(file_get_contents($language), true);
                $baseLocalFile = array_merge($baseLocalFile, $content);
            }
        }
        file_put_contents($file, json_encode($baseLocalFile, JSON_UNESCAPED_UNICODE));
    }
}
