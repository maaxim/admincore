<?php

namespace maaxim\admincore\Events;

use Throwable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ErrorReport
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Throwable $_e;

    public function __construct(Throwable $e)
    {
        $this->_e = $e;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
