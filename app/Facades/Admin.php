<?php


namespace maaxim\admincore\Facades;

/**
 * @method static \maaxim\admincore\Classes\AdminCore getProfileImagesPath()
 * @method static \maaxim\admincore\Classes\AdminCore getUserData(string $key = '')
 * @method static \maaxim\admincore\Classes\AdminCore setUserData(array $data)
 * @method static \maaxim\admincore\Classes\AdminCore setResourceCss(string $path)
 * @method static \maaxim\admincore\Classes\AdminCore setResourceJs(string $path)
 * @method static \maaxim\admincore\Classes\AdminCore getResources()
 * @method static \maaxim\admincore\Classes\AdminCore renderResources()
 * @method static \maaxim\admincore\Classes\AdminCore getUserForData($item)
 *
 * @see \maaxim\admincore\Classes\AdminCore
 */

use Illuminate\Support\Facades\Facade;

class Admin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AdminCore';
    }
}
