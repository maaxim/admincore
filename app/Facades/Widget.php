<?php

namespace maaxim\admincore\Facades;

use Illuminate\Support\Facades\Facade;

class Widget extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Widgets';
    }
}
