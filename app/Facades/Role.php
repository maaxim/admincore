<?php

namespace maaxim\admincore\Facades;

/**
 * @method static BuildRule setRole(Rule $role)
 * @method static BuildRule getRole()
 * @method static BuildRule setAdminPanel($val)
 * @method static BuildRule setGlobalAdd($val)
 * @method static BuildRule setGlobalEdit($val)
 * @method static BuildRule setGlobalDelete($val)
 * @method static BuildRule setPageGroup($group, $abbreviation, PageRule $rules)
 * @method static BuildRule getRules()
 * @method static BuildRule includeMappingFile($file)
 *
 * @see \maaxim\admincore\Classes\Roles\BuildRule
 */

use Illuminate\Support\Facades\Facade;
use maaxim\admincore\Classes\Roles\Rule;
use maaxim\admincore\Classes\Roles\PageRule;
use maaxim\admincore\Classes\Roles\BuildRule;

class Role extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'BuildRule';
    }
}
