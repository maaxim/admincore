<?php

namespace maaxim\admincore\Facades;

/**
 * @method static BuildLink add(array $params = [])
 * @method static BuildLink getLink()
 * @method static BuildLink name(string $name)
 * @method static BuildLink route(string $route)
 * @method static BuildLink icon(string $icon)
 * @method static BuildLink active($routes = '' | [])
 * @method static BuildLink group(\Closure $callback)
 * @method static BuildLink setPageRules($group, $abbreviation, PageRule $rules = null)
 * @method static BuildLink getLinks()
 * @method static BuildLink render()
 * @method static BuildLink includeMappingFile($file)
 *
 * @see \maaxim\admincore\Classes\Links\BuildLink
 */

use Illuminate\Support\Facades\Facade;
use maaxim\admincore\Classes\Roles\PageRule;
use maaxim\admincore\Classes\Links\BuildLink;

class Link extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'BuildLink';
    }
}
