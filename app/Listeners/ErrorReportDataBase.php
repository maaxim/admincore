<?php

namespace maaxim\admincore\Listeners;

use maaxim\admincore\Events\ErrorReport;
use maaxim\admincore\ErrorReport as ErrorReportModel;

class ErrorReportDataBase
{
    /**
     * Handle the event.
     *
     * @param ErrorReport $event
     * @return void
     */
    public function handle(ErrorReport $event)
    {
        if (config('admin.errors_report') == true) {
            $new = new ErrorReportModel();
            $new->file = $event->_e->getFile();
            $new->code = $event->_e->getCode();
            $new->line = $event->_e->getLine();
            $new->message = $event->_e->getMessage();
            $new->save();
        }
    }
}
