<?php

namespace maaxim\admincore\Listeners;

use Illuminate\Support\Facades\Mail;
use maaxim\admincore\Events\ErrorReport;
use maaxim\admincore\Mail\AdminErrorReportMail;

class ErrorReportMail
{
    /**
     * Handle the event.
     *
     * @param ErrorReport $event
     * @return void
     */
    public function handle(ErrorReport $event)
    {
        if (config('admin.errors_report') == true && config('admin.send_email_report') == true) {
            $emails = config('admin.emails');

            if (!is_null($emails)
                && ((is_string($emails) && $emails != '') || (is_array($emails) && count($emails) >= 1))) {
                Mail::send(new AdminErrorReportMail($event->_e));
            }
        }
    }
}
