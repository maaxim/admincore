<?php

namespace maaxim\admincore;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ErrorReport
 * @method static find($id)
 * @method static findOrFail($id)
 * @method static where($col, $dis = '', $val = '')
 * @method static orderBy($col, $direction = 'asc')
 *
 * @property string file
 * @property int code
 * @property int line
 * @property string message
 * @property bool read
 * @property bool fixed
 * @package maaxim\admincore
 */
class ErrorReport extends Model
{
    protected $table = 'error_reports';

    public static function getNewErrorsCount()
    {
        return ErrorReport::where('read', false)->count();
    }

    public static function getErrors()
    {
        return ErrorReport::orderBy('id', 'desc')->limit(3)->get();
    }
}
