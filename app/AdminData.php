<?php

namespace maaxim\admincore;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminData
 * @method static find($id)
 * @method static where($col, $dis = '', $val = '')
 *
 * @property int|mixed|string|null user_id
 * @property int|mixed|string|null data
 * @package maaxim\admincore
 */
class AdminData extends Model
{
    protected $table = 'admin_data';
    protected $primaryKey = 'data_id';
}
