<?php

use maaxim\admincore\Classes\Links\BuildLink;

Link::add()
    ->name('Главная')
    ->route('AdminHome')
    ->icon('fas fa-tachometer-alt');

Link::add()
    ->name('Пользователи')
    ->icon('fas fa-users')
    ->group(function(BuildLink $link) {
        $link->add()
            ->name('Пользователи')
            ->icon('far fa-user')
            ->route('AdminUsers');
        $link->add()
            ->name('Роли')
            ->icon('fas fa-person-booth')
            ->active(['AdminAddRole', 'AdminEditRole'])
            ->route('AdminRoles');
    });
