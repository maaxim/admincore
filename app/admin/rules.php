<?php

use maaxim\admincore\Classes\Roles\PageRule;

// Правила для пользователей
Role::setPageGroup('users', 'Пользователи',
    (new PageRule())->setCustomRule('reset', false, 'Сброс паролей'));

// Правила для ролей
Role::setPageGroup('roles', 'Роли', (new PageRule()));

// Правила для репортов
Role::setPageGroup('reports', 'Ошибка при работе сайта',
    (new PageRule())->setCustomParams([
        'fixed' => [
            'value' => false,
            'abbreviation' => 'Отмечать как "Решено"'
        ]
    ])->setDelete(false));
