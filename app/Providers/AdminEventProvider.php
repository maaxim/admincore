<?php

namespace maaxim\admincore\Providers;

use maaxim\admincore\Events\ErrorReport;
use maaxim\admincore\Listeners\ErrorReportMail;
use maaxim\admincore\Listeners\ErrorReportDataBase;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;

class AdminEventProvider extends EventServiceProvider
{
    protected $listen = [
        ErrorReport::class => [
            ErrorReportDataBase::class,
            ErrorReportMail::class,
        ]
    ];

    public function boot()
    {
        //
    }
}
