<?php


namespace maaxim\admincore\Providers;

use Illuminate\Support\ServiceProvider;
use maaxim\admincore\Classes\AdminCore;
use maaxim\admincore\Classes\Links\BuildLink;
use maaxim\admincore\Classes\Roles\BuildRule;
use maaxim\admincore\Classes\Widgets\Widgets;

class AdminFacadesProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('AdminCore', function ($app) {
            return new AdminCore();
        });

        $this->app->bind('BuildLink', function ($app) {
            return new BuildLink();
        });

        $this->app->bind('BuildRule', function ($app) {
            return new BuildRule();
        });

        $this->app->bind('Widgets', function ($app) {
            return new Widgets();
        });
    }
}
