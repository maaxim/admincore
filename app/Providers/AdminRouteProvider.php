<?php


namespace maaxim\admincore\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AdminRouteProvider extends ServiceProvider
{
    protected string $namespaceBase = 'App\Http\Controllers';
    protected string $namespaceAdmin = 'maaxim\admincore\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapAdminCoreRoutes();
        $this->mapAdminApiRoutes();

        if (file_exists(base_path('routes/admin.php'))) {
            $this->mapAdminRoutes();
        }
    }

    protected function mapAdminCoreRoutes()
    {
        Route::middleware(['web', 'adminUserNotLocked'])
            ->namespace($this->namespaceAdmin)
            ->prefix('admin')
            ->group(__DIR__ . '/../../routes/routes.php');
    }

    protected function mapAdminRoutes()
    {
        Route::middleware(['web', 'adminUserNotLocked', 'isAdmin'])
            ->prefix('admin')
            ->namespace($this->namespaceBase)
            ->group(base_path('routes/admin.php'));
    }

    protected function mapAdminApiRoutes()
    {
        Route::middleware(['api'])
            ->namespace($this->namespaceAdmin)
            ->prefix('admin/api')
            ->group(__DIR__ . '/../../routes/api.php');
    }
}
