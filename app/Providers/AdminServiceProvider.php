<?php

namespace maaxim\admincore\Providers;

use maaxim\admincore\Facades\Link;
use maaxim\admincore\Facades\Role;
use Illuminate\Support\ServiceProvider;
use maaxim\admincore\Http\Middleware\isAdmin;
use maaxim\admincore\Http\Middleware\CheckAccept;
use maaxim\admincore\Http\Middleware\AdminUserNotLocked;

class AdminServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/admin.php', 'admin'
        );

        $this->providers();
        $this->includeRules();

        app('router')->pushMiddlewareToGroup('isAdmin', isAdmin::class);
        app('router')->pushMiddlewareToGroup('adminUserNotLocked', AdminUserNotLocked::class);
        app('router')->pushMiddlewareToGroup('noAccept', CheckAccept::class);

        $this->includeLinks();
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin');

        $this->app->registerDeferredProvider(AdminEventProvider::class);
    }

    protected function providers()
    {
        $this->app->registerDeferredProvider(AdminCommandsProvider::class);
        $this->app->registerDeferredProvider(AdminScheduleProvider::class);
        $this->app->registerDeferredProvider(AdminRouteProvider::class);
        $this->app->registerDeferredProvider(AdminFacadesProvider::class);
        $this->app->registerDeferredProvider(AdminResourcesProvider::class);
    }

    protected function includeRules()
    {
        Role::includeMappingFile(__DIR__ . '/../admin/rules.php');

        foreach (config('admin.rules') as $file) {
            Role::includeMappingFile($file);
        }
    }

    protected function includeLinks()
    {
        Link::includeMappingFile(__DIR__ . '/../admin/links.php');

        foreach (config('admin.links') as $file) {
            Link::includeMappingFile($file);
        }
    }

    protected function includeFile($file)
    {
        if (file_exists($file)) {
            require_once $file;
        }
    }
}
