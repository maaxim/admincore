<?php


namespace maaxim\admincore\Providers;

use maaxim\admincore\Facades\Admin;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Foundation\Application;

class AdminResourcesProvider extends ServiceProvider
{
    protected string $_resource_path;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->_resource_path = $this->replaceResourcePath(config('admin.resource_path'));
    }

    public function register()
    {
        $resourcesCss = [
            '/css/app.css',
        ];
        $resourcesJs = [
            '/js/vendor.js',
            '/js/manifest.js',
            '/js/app.js',
//            '/js/ckeditor/ckeditor.js',
        ];

        $this->includeResourceCss($resourcesCss);
        $this->includeResourceCss(config('admin.resources.css'));
        $this->includeResourceJs($resourcesJs);
        $this->includeResourceJs(config('admin.resources.js'));
    }

    public function boot()
    {
        //
    }

    protected function includeResourceCss($resources)
    {
        foreach ($resources as $resource) {
            Admin::setResourceCss($this->replacePath($resource));
        }
    }

    protected function includeResourceJs($resources)
    {
        foreach ($resources as $resource) {
            Admin::setResourceJs($this->replacePath($resource));
        }
    }

    protected function replaceResourcePath($path): string
    {
        return ($path ? DIRECTORY_SEPARATOR . ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }

    protected function replacePath($path): string
    {
        return $this->_resource_path . DIRECTORY_SEPARATOR
            .ltrim($this->replaceResourcePath($path), DIRECTORY_SEPARATOR);
    }
}
