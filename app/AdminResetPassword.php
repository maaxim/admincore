<?php

namespace maaxim\admincore;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminResetPassword
 * @method static find($id)
 * @method static where($col, $dis = '', $val = '')
 *
 * @property int user_id
 * @property string email
 * @property string token
 * @package maaxim\admincore
 */
class AdminResetPassword extends Model
{
    protected $table = 'password_resets';

    public function updateTimestamps()
    {
        $time = $this->freshTimestamp();

        $createdAtColumn = $this->getCreatedAtColumn();

        if (!$this->exists && !is_null($createdAtColumn) && !$this->isDirty($createdAtColumn)) {
            $this->setCreatedAt($time);
        }
    }
}
