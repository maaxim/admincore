<?php

namespace maaxim\admincore\Mail;

use Throwable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminErrorReportMail extends Mailable
{
    use Queueable, SerializesModels;

    public Throwable $e;

    public function __construct(Throwable $e)
    {
        $this->e = $e;
    }

    /**
     * @return $this
     */
    public function build()
    {
        return $this->markdown('admin::mail.error_report')
            ->subject('На сайте что-то пошло не так | ' . env('APP_NAME'))
            ->from(env('MAIL_FROM_ADDRESS'))
            ->to(config('admin.emails'));
    }
}
