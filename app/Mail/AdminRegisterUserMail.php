<?php

namespace maaxim\admincore\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminRegisterUserMail extends Mailable
{
    use Queueable, SerializesModels;

    public $key;
    public $id;

    protected $_email;

    /**
     * Create a new message instance.
     *
     * @param $key
     * @param $id
     * @param $email
     */
    public function __construct($key, $id, $email)
    {
        $this->key = $key;
        $this->id = $id;
        $this->_email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('admin::mail.register')
            ->subject('Регистрация в AdminPanel | ' . env('APP_NAME'))
            ->from(env('MAIL_FROM_ADDRESS'))
            ->to($this->_email);
    }
}
